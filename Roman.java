class Roman extends Number implements Comparable<Roman>{
    private int _n;
    private String _numeral;
    
    // pre: n > 0 and n <= 4000
    public Roman(int n) {
	if (n <= 0 || n > 4000)
	    throw new ArithmeticException();
	_n = n;
	_numeral = toRoman(n);
    }

    public String toString() {
	return _numeral;
    }

    public void print() {
	System.out.println(this);
    }

    
    private static String toRoman(int n){
	if (n <= 0) return "";
	if (n < 4) return "I" + toRoman(n-1);
	if (n < 5) return "IV";
	if (n < 9) return "V" + toRoman(n-5);
	if (n < 40) return "X" + toRoman(n-10);
	if (n < 50) return "XL" + toRoman(n%10);
	if (n < 90) return "L" + toRoman(n-50);
	if (n < 400) return "C" + toRoman(n-100);
	if (n < 500) return "CD" + toRoman(n%100);
	if (n < 900) return "D" + toRoman(n-500);
	if (n < 4000) return "M" + toRoman(n-1000);
	return "";
    }

    public boolean equals(Object rhs) {
	return (rhs instanceof Roman) && this._n == ((Roman)rhs)._n;
    }

    public int compareTo(Roman rhs) {
	return this._n - rhs._n;
    }


    // Number methods:
    public double doubleValue() {
	return (double)_n;
    }

    public float floatValue() {
	return (float)_n;
    }
    
    public int intValue() {
	return _n;
    }

    public long longValue() {
	return (long)_n;
    }
    
    public static void main (String[] args) {
	Roman a = new Roman(2154);
	System.out.println(a.doubleValue());
	System.out.println(a.floatValue());
	System.out.println(a.intValue());
	System.out.println(a.longValue());
	
    }

}
