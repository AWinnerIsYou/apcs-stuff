public class LoanPayments {
    public static void main (String [] args){
	double t = Double.parseDouble(args[1]);
	double P = Double.parseDouble(args[2]);
	double r = Double.parseDouble(args[3]);
	System.out.println("Payment Balance Interest");
	
	double total, monthlyPayment, balance, interest, priceReduction;
	
	total = P*(Math.exp(r*t));
	monthlyPayment = total/(t*12.0);
	balance = total;
	int i = 0;
	while (balance > 0)
	    {
		interest = P * r * (t/12.0);
		priceReduction = monthlyPayment - interest;
		balance = P - priceReduction;
		P = balance;
		if ( i >= 12)
		    i = 0;
		if ( balance > 0 )
		    {
			StdOut.printf("%6.2f %7.2f %5.2f\n", monthlyPayment, balance, interest);
			i++;
		    }
		else break;
	    }
    }
}
