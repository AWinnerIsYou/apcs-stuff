public class Point{
    private int _x, _y;


    public Point(){
	_x = _y = 0;
    }
    public Point (int x, int y){
	move(x,y);
    }

    public Point (Point other){
	this(other.getX(), other.getY());
    }


    // accessor methods
    public int getX(){
	return _x;
    }


    public int getY(){
	return _y;
    }

    // modifier methods
    public void move(int x, int y){
	_x = x;
	_y = y;
    }

    // postcondition: use StdDraw.java to draw the point. 
    //                Use a black circle to represent the point.
    public void draw(){
	StdDraw.filledCircle(getX(), getY(), 0.25);
    }
    
  
    public String toString(){
	return "(" + _x + ", " + _y + ")"; 
        // return "(" + getX() + ", " + getY() + ")";
    }
    
    // postconditon: return true if the points
    // have equal x and y coordinates.
    public boolean equals(Object other){
	if (other instanceof Point)
	    return (((Point)other).getX() == getX()) 
		&& (((Point)other).getY() == getY());
	return false;
    }

    //postcondition: returns a copy of the point at the same location
    public Point getLocation(){
	return new Point(this);
    }
    
    // postcondtion: translates the point by dx and dy
    //              dx is the change in x
    //              dy is the change in y
    public void translate(int dx, int dy){
	move(getX() + dx, getY() + dy);
    }

    // preconditon: other != null
    // postcondition: sets the location of the point to the specified 
    //                location
    public void setLocation(Point other){
	move(other.getX(), other.getY());
    }

   
    public static void main(String [] args){
	Point a = new Point();
	Point b = new Point(3,5);
	Point c = new Point(b);
	StdDraw.setXscale(-10,10);
	StdDraw.setYscale(-10,10);
	System.out.println(a);
	System.out.println(b);
	System.out.println(c);
	a.draw();
	b.draw();
	c.draw();
    }

}
