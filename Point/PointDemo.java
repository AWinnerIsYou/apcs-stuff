public class PointDemo {
    // precondition:  x != null and y != null
    // postcondition: returns the distance between points x and y
    public static double distance(Point x, Point y) {
	return Math.sqrt(Math.pow(x.getX() - y.getX(),2) +
			 Math.pow(x.getY() - y.getY(),2));
    }
    
    public static void main(String[] args) {
	Point x = new Point(1,2);
	Point y = new Point(4,6);
	System.out.println(distance(x,y));
    }
}
	
