public class ArrayStack<E> implements Stack<E> {
    
    private E[] _stack;
    private int _capacity;
    private int _top; //index position of the top of the stack

    public static final int CAPACITY = 1000;
    
    public ArrayStack(int capacity) {
	_stack = (E[]) new Object[capacity]; //compiler warning
	_capacity = capacity;
	_top = -1;
    }

    public ArrayStack() {
	this(CAPACITY);
    }

    // O(1)
    public void push(E element) throws FullStackException{
	if (size() == _capacity)
	    throw new FullStackException("Stack is Full");
	_stack[++_top] = element;
    }

    // O(1)
    public E pop() throws EmptyStackException {
	if (isEmpty())
	    throw new EmptyStackException("Empty Stack");
	E ans = top();
	_stack[_top--] = null;
	return ans;
    }

    // O(1)
    public E top() throws EmptyStackException {
	if (isEmpty())
	    throw new EmptyStackException("Empty Stack");
	return _stack[_top];
    }

    // O(1)
    public int size() {
	return _top + 1;
    }
    
    //O(1)
    public boolean isEmpty() {
	return size() == 0;
    }

    // tranfers the elements from the stack to rhs until either the stack is
    // empty or rhs is full
    public void transferTo(ArrayStack<E> rhs) {
	while (!isEmpty() && rhs.size() != rhs._capacity) {
	    rhs.push(pop());
	}
    }

    public static void main(String[] args) {
	ArrayStack<Integer> A = new ArrayStack<Integer>(100);
	ArrayStack<Integer> B = new ArrayStack<Integer>(5);
	ArrayStack<Integer> C = new ArrayStack<Integer>(3);
	
	//fill in A
	for (int i = 0 ; i < 100; i++)
	    A.push(1);
	A.transferTo(B);
	B.transferTo(C);
	C.transferTo(A);

	B.transferTo(C);
	A.transferTo(B);
	B.transferTo(C);
     	C.transferTo(A);
	
	

	System.out.println("size of A: " + A.size());
	System.out.println("size of B: " + B.size());
	System.out.println("size of C: " + C.size());
	System.out.println();	

    }
    
}




















