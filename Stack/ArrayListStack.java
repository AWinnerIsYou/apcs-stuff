import java.util.ArrayList;
import java.util.List;

public class ArrayListStack<E> implements Stack<E> {
    
    private ArrayList<E> _stack;
    private int _top;
    
    public ArrayListStack() {
	_stack = new ArrayList<E>();
	_top = -1;
    }

    public void push(E value) {
	_top++;
	_stack.add(value);
    }

    public E top() throws EmptyStackException {
	if (isEmpty())
	    throw new EmptyStackException("Stack is Empty");
	return _stack.get(_top);
    }
    
    public E pop() throws EmptyStackException {
	if (isEmpty())
	    throw new EmptyStackException("Empty Stack");
	E ans = top();
	_stack.set(_top--, null);
	return ans;
    }

    public int size() {
	return _top + 1;
    }

    public boolean isEmpty() {
	return size() == 0;
    }

    public String toString() {
	return _stack.toString();
    }
    

}
