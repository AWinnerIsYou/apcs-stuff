class Node<E> {
    private E _value;
    private Node _next;

    public Node(E value, Node next) {
	_value = value;
	_next = next;
    }
    
    // accessors
    public E getValue() {
	return _value;
    }

    public Node getNext() {
	return _next;
    }

    // modifiers
    public E setValue(E str) {
	E ans = getValue();
	_value = str;
	return ans;
    }
    
    public Node setNext(Node t) {
	Node ans = getNext();
	_next = t;
	return ans;
    }

    public String toString() {
	return getValue().toString();
    }

}


