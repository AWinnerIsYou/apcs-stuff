public class MatchParen{

    public final static String OPENING = "([{";
    public final static String CLOSING = ")]}";
    public final static String PARENTHESES = OPENING + CLOSING;


    // returns true if token is contained within exp; false otherwise
    // e.g.  contains("mississippi","is") -> true
    //       contains("mississippi","Is") -> false
    //       contains("mississippi","ippi") -> true
    public static boolean contains(String exp, String token){
	return exp.indexOf(token) != -1;
    }


    // post: removes all whitespace characters and non-parentheses
    //       from exp.
    public static String filterParen(String exp){
	//System.out.println("filtering parens:");
	String ans = "";
	for (int i = 0; i < exp.length(); i++) 
	    if (contains(PARENTHESES, exp.substring(i, i+1)))
		ans += exp.substring(i,i+1);
	//System.out.println("filtered: " + ans);
	//System.out.println();
	return ans;
    }

    // Uses a stack to check if exp has matching parentheses.
    // pre: is empty or only contains parentheses.
    public static boolean match(String exp){
	if (exp.length() == 0) return false;
	if (exp.length() % 2 == 1) return false;
	
	Stack<String> S = new NodeStack<String>();
	for (int i = 0; i < exp.length(); i++) {
	    String curr = exp.substring(i,i+1);

	    if (contains(OPENING, curr)) S.push(curr);
	    else if (contains(CLOSING, curr)) {
		if (S.isEmpty()) return false;
		
		if (S.top().equals(oppositeParen(curr))) S.pop();
		else return false;
	    }

	}	
	return S.isEmpty();
    }
    
    private static String oppositeParen(String s) {
	if (contains(OPENING,s)) {
	    int i = OPENING.indexOf(s);
	    return CLOSING.substring(i,i+1);
	}
	
	if (contains(CLOSING,s)) {
	    int i = CLOSING.indexOf(s);
	    return OPENING.substring(i,i+1);
	}
	return null;
    }
    
    // Assuming ()[]{} are the only characters
    public static void main(String [] args){
	/*
	System.out.println("opp of (:" + oppositeParen("("));
	System.out.println("opp of ):" + oppositeParen(")"));
	System.out.println("opp of [:" + oppositeParen("["));
	System.out.println("opp of ]:" + oppositeParen("]"));
	System.out.println("opp of {:" + oppositeParen("{"));
	System.out.println("opp of }:" + oppositeParen("}"));
	*/
	String exp = "";

	System.out.print("> ");
	exp = StdIn.readLine();
	while ( !contains(exp,"-1")){
	    if (match(filterParen(exp)))
		System.out.println("Match");
	    else
		System.out.println("Don't match");
	    System.out.print("> ");
	    exp = StdIn.readLine();
	}
    }

}
