import java.util.List;
import java.util.ArrayList;

public class Client {
    
    public static<E> void reverse(List<E> L){
        int N = L.size();
        Stack<E> S = new ArrayStack<E>(N);
        for (int i = 0; i < N; i++) S.push(L.get(i));
        for (int i = 0; i < N; i++) L.set(i,S.pop());	
    }

    public static void main(String[] args) {
	/*
	System.out.println("Reverse Algo:");
        List<Integer> L = new ArrayList<Integer>();
        for (int i = 0; i < 11; i++)
            L.add(i);
        System.out.println(L);
        reverse(L);
        System.out.println(L);
	*/
	
	

    }
    

}
