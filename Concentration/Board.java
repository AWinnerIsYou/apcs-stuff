import java.util.ArrayList;
import java.util.List;


/*
  Concentration is a game in which tiles are placed faced-down in rows on a 
  "concentration board." Each tile contains an image. For each tile, there is
  exactly one other tile with the same image. The player is to pick a tile, see
  which image it has, and try to find its match from the remaining face-down
  tiles. If the player fails to turn over its match, both tiles are turned 
  face-down and the player attempts to pick a matching pair again. The game is
  over when all tiles on the board are turned face-up.
  
  In this implementation of a simplified version of concentration,
  * Images will be strings chosen from a FooList. 
  * The concentration board will contain an even number of Tiles.
  * The Board will be implemented  as a one-dimensional array of Tiles.
  
  For example, if the size of the concentration board is requested to be 4, the
  board will have 16 Tiles. The one-dimensional array, gameboard, can be viewed
  as a 4-by-4 concentration board as follows:
  
  gameboard[0]		 gameboard[1]		gameboard[2]	gameboard[3]
  gameboard[4]	         gameboard[5]		gameboard[6]	gameboard[7]
  gameboard[8] 	         gameboard[9]		gameboard[10]	gameboard[11]
  gameboard[12]	         gameboard[13]		gameboard[14]	gameboard[15]
  
An incomplete implementation of the Board class appear below.
*/

public class Board{
    private ArrayList<Tile> _gameBoard; // concentration board of Tiles
    private int _size;   // number of Tiles on board
    private int _rowLength; // number of Tiles printed in a row
    private int _numberOfTilesFaceUp; // number of Tiles face-up
    private FooList _possibleTileValues; // possible Tile images
    
    // Constructs n by n concentration board of Tiles whose values
    // are chosen from the already filled FooList list.
    // Precondition: n is the length of a side of the board.
    // n is an even positive integer.
    // FooList contains at least n * n / 2 strings.
    public Board(int n, FooList list){
	_gameBoard = new ArrayList<Tile>();
	_size = n*n;
	_rowLength = n;
	_numberOfTilesFaceUp = 0;
	_possibleTileValues = list;
	fillBoard();
	for (Tile t : _gameBoard)
	    t.turnFaceDown();
    }
    
    
    // Randomly fils this concentration board with tiles. The number
    // of distinct tiles used on the board is size / 2.
    // Any one tile image appears exactly twice.
    // Precondition: number of positions on board is even.
    // possibleTileValues contains at least size / 2 elements.
    private void fillBoard(){
	for (int i = 0; i < _size / 2; i++) {
	    String tile = _possibleTileValues.removeRandomFoo();  
	    _gameBoard.add(new Tile(tile));
	    _gameBoard.add(new Tile(tile));
	}
	//shuffle _gameboard
    }

    private void shuffle() {
	for (int i = 0; i < _size; i++) {
	    int r = (int) (Math.random() * _size) + i;
	    _gameBoard.set(i,_gameBoard.set(r,_gameBoard.get(i)));
	}
    }
    
    //Precondition: Tile in position p is face-down.
    //Postcondition: Tile in postion p is face-up.
    public void lookAtTile(int p){
	_gameBoard.get(p).turnFaceUp();
    }
    
    //Checks whether the Tile in pos1 and pos2 have the same image.
    // If they do, the Tiles are turned face-up. If not, the Tileas
    // are turned face-down.
    // Precondition: _gameBoard[pos1] is face-up.
    // _gameBoard[pos2] is face-up.
    public void checkMatch(int pos1, int pos2){
	if (pos1 == pos2) {
	    pickTile(pos1).turnFaceDown();
	    _numberOfTilesFaceUp--;
	}
	else if (pickTile(pos1).showFace().equals(pickTile(pos2).showFace())) {
	    lookAtTile(pos1);
	    lookAtTile(pos2);
	    _numberOfTilesFaceUp -= 2;
	}
	else {
	    _gameBoard.get(pos1).turnFaceDown();
	    _gameBoard.get(pos2).turnFaceDown();
	}
    }
    
    // Board is printed for the player. If the Tile is turned face-up,
    // the image is printed. If the Tile is turned face-down,
    // the Tile position is printed.
    public void printBoard(){
	for (int i = 0; i < _size; i++) {
	    if (i % _rowLength == 0)
		System.out.println();
	    Tile t = pickTile(i);
	    if (t.isFaceUp())
		System.out.print(format(t.showFace(), t.showFace().length() + 2));
	    else
		System.out.print(format(i + "", t.showFace().length() + 2));
	    
	    
	}
	System.out.println();
    }
    
    // Returns Tile in postion pos.
    public Tile pickTile(int pos){
	return _gameBoard.get(pos);
    }
    
    // Returns right-justified number with p places as a string.	
    public String format(String word, int p){
	String ans = word;
	while (ans.length() < p)
	    ans = " " + ans;
	return ans;
    }
    
    // Returns true if all Tiles are turned face-up, false otherwise.
    public boolean allTiles(){
	for (int i = 0; i < _size; i++)
	    if (pickTile(i).isFaceUp() == false)
		return false;
	return true;
    }
    public static void main(String[] args) {
	String[] foos = {"cat", "dog"};
	FooList hi = new FooList(foos, 3);
	Board board = new Board(2, hi);
	board.printBoard();
	board.checkMatch(0,2);
	board.printBoard();
	
    }
}
