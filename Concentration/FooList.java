import java.util.ArrayList;
import java.util.List;


/*
A FooList is a class that defines a list of strings and a string length.
All strings in a FooList have the same length. There is no preset number
of strings that a FooList can hold.

For example,

The FooList list1 may contain the strings aa,bb,cc and dd (all strings of 
length 2).

The Foolist list2 may contain the strings cat,dog,pig,fox,bat, and eel (all
strings of length 3).

An incomplete implementation of the FooList class appears below.
*/


public class FooList{

    // private instance variables declared here
    private int _fooLength;
    private ArrayList<String> _availableFoos;
    
    //Constructor implementation initializes 
    //_fooLength (the length of the strings in FooList's list) and
    //_availableFoos (FooList's list of strings).

    // O(n)
    public FooList(String [] foos,int len){
	_fooLength = len;
	_availableFoos  = new ArrayList<String>();
	fillFooList(foos);
    }
    
    
    
    //PostCondition: Returns true if the string key, is found in the 
    //FooList's list of strings, false otherwise.
    // O(n)
    public boolean found(String key){
	for(String foo : _availableFoos)
	    if(foo.equals(key))
		return true;
	return false;
    }
    
    //Adds the string, entry, to FooList's list implementation if
    //it is the correct length and not already in the list. If the
    //string is already in the list or if the string is not the 
    //correct length, it is not added and false is returned.
    // O(n)
    public boolean addFoo(String entry){
	if (found(entry) || entry.length() != _fooLength)
	    return false;
	return _availableFoos.add(entry);
    }
    
    //Removes and returns a random string entry from FooList's
    //list of strings.
    // O(n)
    public String removeRandomFoo(){
	int r = (int)(Math.random() * _availableFoos.size());
	return _availableFoos.remove(r);
    }
    
    //Returns the string in position i of FooList's list
    //implementation. The first string is in position 0.
    // O(1)
    public String getFoo(int i){
	return _availableFoos.get(i);
    }
    
    //Returns length of a foo.
    // O(n)
    public int getLength(){
	return _fooLength;
    }
    
    // Fills the FooList's list with the strings in foos
    // O(n)
    public void fillFooList(String [] foos){
	for (int i = 0; i < foos.length; i++)
	    addFoo(foos[i]);
    }
    
    // O(n)
    public String toString(){
	return _availableFoos.toString();
    }
    
    // O(1)
    public int size() {
	return _availableFoos.size();
    }

    // O(1)
    public boolean isEmpty() {
	return size() == 0;
    }
    
    // O(n)
    public boolean equals(Object rhs) {
	return ((rhs instanceof FooList) &&
		(((FooList) rhs)._availableFoos.equals(_availableFoos)));
    }
	
    
	
    public static void main(String [] args){
	String [] foos = {"cat","bat","eel","dog","cow","pig","cat","crow"};
	FooList fooList = new FooList(foos,3);
	FooList fool = new FooList(foos,3);
	/*
	  System.out.println(fooList);
	  while (!fooList.isEmpty()){
	  System.out.println(fooList.removeRandomFoo());
	  System.out.println(fooList);
	  }
	*/

	System.out.println(fooList.equals(fool));
    }
    
}



