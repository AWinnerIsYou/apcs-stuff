public class Sum {
    public static int sum(int n) {
	return n * (n + 1) / 2;
    }

    public static int sumR(int n) {
	if (n == 1) return 1;
	return n + sumR(n-1);
    }

    public static int sumI(int n) {
	int ans = 0;
	for (int i = 1; i <= n; i++) 
	    ans += i;
	return ans;
	    
    }



    public static int sum(int a, int d, int n) {
	return n * (a + (n-1)*d / 2);
    }

    public static int sumI(int a, int d, int n) {
	int ans = a;
	for (int i = d; i < n*d ; i+= d)
	    ans += a + i;
	return ans;

    }

    public static int sumR(int a, int d, int n) {
	if (n == 1) return a;
	return a + (n-1)*d + sumR(a,d,n-1);	
    }

    public static void main(String[] args) {
	System.out.println(sum(2,3,5));
	System.out.println(sumI(2,3,5));
	System.out.println(sumR(2,3,5));
    }

}
