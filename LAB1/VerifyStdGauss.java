public class VerifyStdGauss {
    public static void main(String[] args) {
	int N, i, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14;
	double u, v, w;
	r1= r2 = r3 = r4 = r5 = r6 =r7 =r8 = r9 = r10 = r11 = r12 = r13 = r14 = 0;
	i = 0;
	N = Integer.parseInt(args[0]); 
	while (i != N) {
	    u = Math.random();
	    v = Math.random();
	    w = Math.sin(2 * Math.PI * v) * Math.sqrt(-2 * Math.log(u));
	    if (w < -3.0) r1++;
	    else if (w < -2.5) r2++;
	    else if (w < -2.0) r3++;
	    else if (w < -1.5) r4++;
	    else if (w < -1.0) r5++;
	    else if (w < -0.5) r6++;
	    else if (w < 0.0) r7++;
	    else if (w < 0.5) r8++;
	    else if (w < 1.0) r9++;
	    else if (w < 1.5) r10++;
	    else if (w < 2.0) r11++;
	    else if (w < 2.5) r12++;
	    else if (w < 3.0) r13++;
	    else r14++;
	    i++;
	}
	double M = N * 0.01;
	System.out.println("       <-3.0: " + r1 / M + "%\t0.1%");
	System.out.println("-3.0 to -2.5: " + r2 / M + "%\t0.5%");
	System.out.println("-2.5 to -2.0: " + r3 / M + "%\t1.7%");
	System.out.println("-2.0 to -1.5: " + r4 / M + "%\t4.4%");
	System.out.println("-1.5 to -1.0: " + r5 / M + "%\t9.2%");
	System.out.println("-1.0 to -0.5: " + r6 / M + "%\t15.0%");
	System.out.println(" -0.5 to 0.0: " + r7 / M + "%\t19.1%");
	System.out.println("  0.0 to 0.5: " + r8 / M + "%\t19.1%");
	System.out.println("  0.5 to 1.0: " + r9 / M + "%\t15.0%");
	System.out.println("  1.0 to 1.5: " + r10 / M + "%\t9.2%");
	System.out.println("  1.5 to 2.0: " + r11 / M + "%\t4.4%");
	System.out.println("  2.0 to 2.5: " + r12 / M + "%\t1.7%");
	System.out.println("  2.5 to 3.0: " + r13 / M + "%\t0.5%");
	System.out.println("        >3.0: " + r14 / M + "%\t0.1%");
    }
}
