public class FiveMedian {
    public static void main(String[] args) {
	int a,b,c,d,e,i;
	a = Integer.parseInt(args[0]);
	b = Integer.parseInt(args[1]);
	c = Integer.parseInt(args[2]);
	d = Integer.parseInt(args[3]);
	e = Integer.parseInt(args[4]);
	i = 1;
	int[] arr = {a, b, c, d, e};
	while (i < 5) {
	    int index = arr[i];
	    int j = i;
	    while (j > 0 && arr[j-1] > index) {
		arr[j] = arr[j-1];
		j--;
	    }
	    arr[j] = index;
	    i++;
	}
	System.out.println("median: " + arr[2]);
    }
}
	  		    