public class DotProduct {
    public static void main(String[] args) {
	double[] x = {0.1, 0.4, 0.5};
	int[] y = {100, 80, 90};
	double dot = 0.0;
	for (int i = 0; i < x.length; i++) dot += x[i]*y[i];
	System.out.println(dot);
    }
}
