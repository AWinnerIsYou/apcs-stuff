public class BarCode implements Comparable{
    // instance variables
    private String _zip;
    private int _checkDigit;

    // constructors
    //precondtion: zip.length() = 5 and zip contains only digits.
    //postcondition: throws a runtime exception zip is not the correct length
    //               or zip contains a non digit
    //               _zip and _checkDigit are initialized.
    public BarCode(String zip) {
	if (zip.length() != 5) throw new IllegalArgumentException("zip code must have length 5");
	for (int i = 0; i < 5; i++)
	    if ("1234567890".indexOf(zip.substring(i,i+1)) == -1)
		throw new IllegalArgumentException("zip code must contain digits only");
	_zip = zip;
	
	for (int i = 0; i < 5; i++) {
	    _checkDigit += Integer.parseInt(zip.substring(i,i+1));
	}
	_checkDigit %= 10;
				   
    }
    
    // postcondition: Creates a copy of a bar code.
    public BarCode(BarCode x){
	this(x._zip);
    }


    //post: computes and returns the check sum for _zip
    private int checkSum(){
	return _checkDigit;
    }

    //postcondition: format zip + check digit + barcode 
    //ex. "084518  |||:::|::|::|::|:|:|::::|||::|:|"      
    public String toString(){
	String ret = _zip + _checkDigit + " |";
	for (int i = 0; i < 6; i++) {
	    String curr = (_zip + _checkDigit).substring(i,i+1);
	    if ("1".equals(curr)) ret += ":::||";
	    else if ("2".equals(curr)) ret += "::|:|";
	    else if ("3".equals(curr)) ret += "::||:";
	    else if ("4".equals(curr)) ret += ":|::|";
	    else if ("5".equals(curr)) ret += ":|:|:";
	    else if ("6".equals(curr)) ret += ":||::";
	    else if ("7".equals(curr)) ret += "|:::|";
	    else if ("8".equals(curr)) ret += "|::|:";
	    else if ("9".equals(curr)) ret += "|:|::";
	    else if ("0".equals(curr)) ret += "||:::";
	}
	ret += "|";
	return ret;
    }



    public boolean equals(Object other){
	return (other instanceof BarCode &&
		(((BarCode)other)._zip).equals(_zip));
    }

    // postcondition: compares the zip + checkdigit 
    //  ex 084518 < 193418 true
    //     084528 > 000000 true
    public int compareTo(Object other){
	int thsZip = Integer.parseInt(_zip + _checkDigit);
	int othZip = Integer.parseInt(((BarCode)other)._zip + ((BarCode)other)._checkDigit);
	return thsZip - othZip;
    }

    // postcondition: Uses StdDraw to draw the bar code. 
    public void draw(){
	// i have no fricking clue :)
    }

}
