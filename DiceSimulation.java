public class DiceSimulation {
    public static void printArray(double[] a) {
	System.out.print("{");
	for (int i = 0; i < 12; i++) System.out.print(a[i] + ", ");
	System.out.println(a[12] + "}");
    }

    public static void main(String[] args) {
	double[] dist = new double[13]; 
	for (int i = 1; i <= 6; i++) 
	    for (int j = 1; j <= 6; j++) 
		dist[i+j] += 1.0;
 
	for (int k = 1; k <= 12; k++) 
	    dist[k] /= 36.0; 

	printArray(dist);
	System.out.println();
	
	int N = Integer.parseInt(args[0]);
	double[] output = new double[13];
	for (int count = 0; count != N; count++) {
	    int a = (int)(Math.random() * 6 + 1);
	    int b = (int)(Math.random() * 6 + 1);
	    output[a+b] += 1.0;
	}
	for (int count = 0; count < output.length; count++) {
	    output[count] /= N;
	}
	printArray(output);
    }
}
