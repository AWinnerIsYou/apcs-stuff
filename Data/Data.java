public class Data {

    // precondition: 0 < k < data.length-1
    public static boolean isMode(int[] data, int k){
	return (data[k] > data[k-1]) && (data[k] > data[k+1]);
    }

    // precondition:  data is unimodal and data.length ≥ 3
    public static int modeIndex(int[] data) {
	for (int i = 1; i < data.length; i++) 
	    if (isMode(data,i))
		return i;
	return -1;
    }
    
    // precondition:  data is unimodal and data.length ≥ 3;
    //                data[k] ≥ 0 for 0 ≤ k < data.length
    public static void printHistogram(int[] data, int longestBar, String barChar){
	for (int i = 0; i < data.length; i++) {
	    for (int j = 0; j < (int)data[i]*longestBar/data[modeIndex(data)]; j++)
		System.out.print(barChar);
	    System.out.println();
	}
	
    }
    
    public static void main(String[] args) {
	int[] A = {3,5,9,10,12,11,9,4};
	System.out.println(isMode(A,4)); //true
	System.out.println(isMode(A,6)); //false
	System.out.println(modeIndex(A));//4
	printHistogram(A, 20, "x");
    }

}
