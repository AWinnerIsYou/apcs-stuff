public class BadShuffle {

    public static void printArray(int[][] a) {
	for (int i = 0; i < a.length; i++) {
	    for (int j = 0; j < a.length; j++) System.out.print(a[i][j] + " ");
	    System.out.println();
	}
    }

    public static void badShuffle(int[] arr) {
	for (int i = 0; i < arr.length; i++) 
	    { 
		int rand = 0 + (int) (Math.random() * (arr.length-i)); 
		int temp = arr[rand]; 
		arr[rand] = arr[i]; 
		arr[i] = temp; 
	    } 
    }



    public static void main(String[] args) {
	
	int M = Integer.parseInt(args[0]); //size of array
	int N = Integer.parseInt(args[1]); //number of shuffles
	
	int[][] trials = new int[M][M]; //trials array
	
	//set array values:
	int[] arr = new int[M];
	for (int i = 0; i < M; i++) arr[i] = i;

	//run trials:
	for (int k = 0; k < N; k++) { //trials
	    badShuffle(arr);
	    for (int i = 0; i < M; i++) {
		trials[arr[i]][i] += 1;
	    }
	}
	printArray(trials);
	System.out.println("Each term should be about: " + (double)N/M);
    }
}
