public class Rumors {

    
    public static void main(String[] args) {
	int N = Integer.parseInt(args[0]); //number of guests;
	int M = Integer.parseInt(args[1]); //trials
	boolean[] guests = new boolean[N]; //guests have not heard the rumor if false
	guests[0] = true; //bob

	int total_heard = 0;

	for (int j = 0; j < M; j++) {
	    for (int i = 0; i < guests.length-1; i++) { //spread rumor
		if (Math.random() < (double)(guests.length-i-1)/(guests.length-1)) guests[i+1] = true;
		else break;
	    }
	    for (int i = 0; i < guests.length; i++) {
		if (guests[i]) total_heard++;
	    }
	    for (int i = 1; i < guests.length; i++) {
		guests[i] = false;
	    }
	}
	double average_heard = (double)total_heard/M;
	System.out.println("Probability: " + 100*(double)average_heard/N + "%");
	System.out.println("Out of " + M + " trials with " + N + " guests, the average number of guests who heard the rumor was " + average_heard + ".");
    }
}
