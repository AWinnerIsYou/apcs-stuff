public class TreeApp {
    
    public static<E> void traverseInOrder(TreeNode<E> root) {
	if (root == null)
	    return; 
	traverseInOrder(root.getLeft());
	System.out.print(root.getValue() + " ");
	traverseInOrder(root.getRight());
    }

    public static<E> void traversePreOrder(TreeNode<E> root) {
	if (root == null)
	    return; 
	System.out.print(root.getValue() + " ");
	traversePreOrder(root.getLeft());
	traversePreOrder(root.getRight());
    }

    public static<E> void traversePostOrder(TreeNode<E> root) {
	if (root == null)
	    return; 
	traversePostOrder(root.getLeft());
	traversePostOrder(root.getRight());
	System.out.print(root.getValue() + " ");
    }

    public static<E> int countNodes(TreeNode<E> rt) {
	if (rt == null) return 0;
	else return 1 + countNodes(rt.getLeft()) + countNodes(rt.getRight());
    }
    
    public static<E> boolean isLeaf(TreeNode<E> rt) {
	return (rt != null && (rt.getLeft() == null && rt.getRight() == null));
    }
    
    public static<E> int height(TreeNode<E> rt) {
	if (rt == null || isLeaf(rt)) return 0;
	return 1 + Math.max(height(rt.getLeft()),height(rt.getRight()));
    }

    public static void main(String[] args) {
	TreeNode<Integer> root = new TreeNode<Integer>(1,null,null);
	//              1
	//            /   \
	//           2     3
	//            \   / \
	//             4 5   6
	//              / \
	//             7   8

	root.setLeft(new TreeNode<Integer>(2,null,null));
	root.setRight(new TreeNode<Integer>(3,null,null));
	root.getLeft().setRight(new TreeNode<Integer>(4,null,null));
	root.getRight().setLeft(new TreeNode<Integer>(5,null,null));
	root.getRight().setRight(new TreeNode<Integer>(6,null,null));
	root.getRight().getLeft().setLeft(new TreeNode<Integer>(7,null,null));
	root.getRight().getLeft().setRight(new TreeNode<Integer>(8,null,null));
	
	traverseInOrder(root); //2 4 1 7 5 8 3 6
	System.out.println();
	traversePreOrder(root); //1 2 4 3 5 7 8 6
	System.out.println();
	traversePostOrder(root); // 4 2 7 8 5 6 3 1
	System.out.println();
	System.out.println(countNodes(root));
	System.out.println(height(root));
    }

}
