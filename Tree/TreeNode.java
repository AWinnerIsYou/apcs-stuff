public class TreeNode<E>{

    private E _value;
    private TreeNode<E> _left, _right;


    // ******   Constructors ******
    public TreeNode(E value){
	this(value,null,null);
    }

    public TreeNode(E value, TreeNode<E> left, TreeNode<E> right){
	_value = value;
	_left = left;
	_right = right;
    }

    
    // ***** Accessor Methods *****
    public E getValue(){
	return _value;
    }

    public TreeNode<E> getLeft(){
	return _left;
    }
    
    public TreeNode<E> getRight(){
	return _right;
    }



    // ***** Mutator Methods *****
    public E setValue(E newValue){
	E ans = getValue();
	_value = newValue;
	return ans;
    }

    public TreeNode<E> setLeft(TreeNode<E> newLeft){
	TreeNode<E> ans = getLeft();
	_left = newLeft;
	return ans;
    }


    public TreeNode<E> setRight(TreeNode<E> newRight){
	TreeNode<E> ans = getRight();
	_right = newRight;
	return ans;
    }

    // ***** Other methods ****
    public boolean isLeaf(){
	return getLeft() == null && getRight() == null;
    }



    public String toString(){
	return getValue() + "";
    }


}
