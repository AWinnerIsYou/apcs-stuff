public class BST<E extends Comparable>{ // implements BST<Comparable>{

    private TreeNode<E> _root;
    private int _size;


    // **** Constructor *****
    public BST(){
	_root = null;
	_size = 0;
    }


    // returns false if val not in the tree
    // returns true an removes one occurrence of val from the tree.
    // This method should run O(log N) if the tree is balanced.
    public boolean remove(E val){
	if (isEmpty()) return false;
	TreeNode<E> found = find(val);
	if (found == null) return false;
	if (found.isLeaf()) {
	    found = null;
	    return true;
	}
	if (found.getRight() == null) {
	    TreeNode<E> maxLeftNode = maxNode(found.getLeft());
	    E rep = maxLeftNode.getValue();
	    remove(rep);
	    found.setValue(rep);
	    return true;
	}
	else {
	    TreeNode<E> minRightNode = minNode(found.getRight());;
	    E rep = minRightNode.getValue();
	    remove(rep);
	    found.setValue(rep);
	    return true;

	}
    }
   

    public boolean isEmpty(){
	return _size == 0;
    }

    public int size() {
	return _size;
    }


    // returns null if val not found
    public TreeNode<E> find(E val){
	TreeNode<E> curr = _root;
	while ( curr != null && !val.equals(curr.getValue())){
	    if (val.compareTo(curr.getValue()) < 0) curr = curr.getLeft();
	    else curr = curr.getRight();
	}
	return curr;
    }
    
    // post: return null if x not found
    public TreeNode<E> findR(E x){
	return findR(_root, x);
    }

    private TreeNode<E> findR(TreeNode<E> rt, E x){
	if (rt == null || x.equals(rt.getValue())) return rt;
	if (x.compareTo(rt.getValue()) < 0) 
	    return findR(rt.getLeft(),x);
	return findR(rt.getRight(), x);
    }


    public boolean isFound(E x){
	return find(x) != null;
    }


    // pre: !isEmpty()
    public E maxValue() throws IllegalStateException{
	return maxValue(_root);
    }

    private E maxValue(TreeNode<E> rt) throws IllegalStateException{
	if (rt == null) throw new IllegalStateException("empty");
	return maxNode(rt).getValue();
    }

    private TreeNode<E> maxNode(TreeNode<E> rt){
	if (rt != null) 
	    while (rt.getRight() != null)
		rt = rt.getRight();
	return rt;
    }

    // pre: !isEmpty()
    public E minValue() throws IllegalStateException{
	return minValue(_root);
    }

    private E minValue(TreeNode<E> rt) throws IllegalStateException{
	if (rt == null) throw new IllegalStateException("empty");	
	return minNode(rt).getValue();
    }

    private TreeNode<E> minNode(TreeNode<E> rt){
	if (rt != null) 
	    while (rt.getLeft() != null) 
		rt = rt.getLeft();
	return rt;
    }
    


    public void insertR(E x){
	_root = insertR(_root,new TreeNode<E>(x));
	_size++;
    }

    private TreeNode<E> insertR(TreeNode<E> rt, TreeNode<E> leaf){
	if (rt == null) return leaf;
	E val = leaf.getValue();
	if (val.compareTo(rt.getValue()) < 0)
	    rt.setLeft(insertR(rt.getLeft(),leaf));
	else 
	    rt.setRight(insertR(rt.getRight(),leaf));
	return rt;
    }



    private void insert(E val){
	TreeNode<E> leaf = new TreeNode<E>(val);
	if (_root == null)
	    _root = leaf;
	else{
	    TreeNode<E> curr = _root;
	    TreeNode<E> prev = curr;
	    while (curr != null){
		prev = curr;
		if (val.compareTo(curr.getValue()) >= 0)
		    curr = curr.getRight();
		else
		    curr = curr.getLeft();
	    }
	    if (val.compareTo(prev.getValue()) >= 0)
		prev.setRight(leaf);
	    else
		prev.setLeft(leaf);

	}
	_size++;
    }

   
    public void inorder(){
	System.out.print("inorder: " );
	inorderTraversal(_root);
	System.out.println();
    }

    

    private void inorderTraversal(TreeNode<E> rt){
	if (rt == null) return;
	inorderTraversal(rt.getLeft());
	System.out.print(rt.getValue() + " " );
	inorderTraversal(rt.getRight());
    }
   
    public void preorder(){
	System.out.print("preorder: " );
	preorderTraversal(_root);
	System.out.println();
    }

    private void preorderTraversal(TreeNode<E> rt){
	if (rt == null) return;
	System.out.print(rt.getValue() + " " );	
	preorderTraversal(rt.getLeft());
	preorderTraversal(rt.getRight());
    }

    public void postorder(){
	System.out.print("postorder: " );
	postorderTraversal(_root);
	System.out.println();
    }

    private void postorderTraversal(TreeNode<E> rt){
	if (rt == null) return;
	postorderTraversal(rt.getLeft());
	postorderTraversal(rt.getRight());
	System.out.print(rt.getValue() + " " );	    
    }

    public static void main(String [] args){
	BST<Integer> tree = new BST<Integer>();
	int N = 10; //Integer.parseInt(args[0]);
	for (int i = 0; i < N; i++){
	    int r = (int) (Math.random() * 10);
	    System.out.println(r + " is found: " + tree.isFound(r));
	    System.out.println("insert " + r);
	    tree.insert(r);
	    tree.inorder();
	    tree.preorder();
	    tree.postorder();
	    System.out.println("Max value: " + tree.maxValue());
	    System.out.println("Min value: " + tree.minValue());
	    System.out.println();
	}

    }

}
