

Remove Algorithm for a BST
==========================
  Removing val from a BST.
	
  Steps
  1. if the tree is empty then stop.
  2. if the val is a leaf remove the leaf.
  3. if the val is an internal node with no right subtree,
     then transfer the maximum value,max, from the left subtree into the node
     that contains val, then remove max from the left subtree.  
  4. otherwise transfer the minimum value, min, from the right subtree
     into the node that contains val, then remove min from the right subtree.


Exercises:
==========

E1. Draw the BST from this sequence: 10, 25, 5, 4, 13, 11, 7, 12
                                     10                                  
                                5           25                               
                              4   7       13                                
                                        11                                   
                                         12                               
                                                                        
E2. Write the sequences produced by the preorder, postorder and inorder
    traversals.
    inorder: 4 5 7 10 11 12 13 25
    preorder: 10 5 4 7 25 13 11 12
    postorder: 4 7 5 12 11 13 25 10

E3. What does the tree look like after removing the 13.
                                     10                                  
                                5           25                               
                              4   7       12                                
                                        11                                                                                                       
E4. What does the tree look like after removing the 5.
                                     10                                  
                                7           25                               
                              4           13                                
                                        11                                   
                                         12                               

E5. What does the tree look like after removing the 10.
                                     11                                  
                                5           25                               
                              4   7       13                                
                                        12                                   


P11. Write the BST method:

     // returns false if val not in the tree
     // returns true an removes one occurrence of val from the tree.
     // This method should run O(log N) if the tree is balanced.
     public boolean remove(E val){}




P12. Write the toString() method for the BST class. The string returned
     should be the a nondecreasing sequence. Each value should be seperated
     by commas.

      For example:    "2"    if the tree is size = 1
                     "2, 3"  if the tree is size = 2 with 3 at the root
		     




