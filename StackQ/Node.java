class Node<E> {
    private E _value;
    private Node<E> _next;

    public Node(E value, Node<E> next) {
	_value = value;
	_next = next;
    }
    
    // accessors
    public E getValue() {
	return _value;
    }

    public Node<E> getNext() {
	return _next;
    }

    // modifiers
    public E setValue(E str) {
	E ans = getValue();
	_value = str;
	return ans;
    }
    
    public Node<E> setNext(Node<E> t) {
	Node<E> ans = getNext();
	_next = t;
	return ans;
    }

    public String toString() {
	return getValue().toString();
    }

}


