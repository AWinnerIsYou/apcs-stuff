public class StackQ<E> implements Stack<E> {
    
    private NodeQueue<E> _queue1, _queue2;

    public StackQ() {
	_queue1 = new NodeQueue<E>();
	_queue2 = new NodeQueue<E>();
    }

    public void push(E element) {
	_queue2.enqueue(element);
	while (!_queue1.isEmpty())
	    _queue2.enqueue(_queue1.dequeue());
	
	NodeQueue<E> temp = _queue1;
	_queue1 = _queue2;
	_queue2 = temp;

    }
    
    public E pop() throws EmptyStackException{
	if (size() == 0) throw new EmptyStackException("Stack is Empty");
	return _queue1.dequeue();
    }

    public E top() throws EmptyStackException {
	if (size() == 0) throw new EmptyStackException("Stack is Empty");
	return _queue1.front();
    }
	
    public int size() {
	return _queue1.size();
    }

    public boolean isEmpty() {
	return size() == 0;
    }

    public static void main(String [] args) {
	StackQ<Integer> S = new StackQ<Integer>();
	S.push(1);
	S.push(2);
	S.push(3);
	S.push(4);
	S.push(5);
	System.out.println(S.pop());
	System.out.println(S.pop());
	System.out.println(S.pop());
	System.out.println(S.pop());
	System.out.println(S.pop());
	
    }

}
