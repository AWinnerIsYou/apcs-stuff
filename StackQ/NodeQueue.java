public class NodeQueue<E> implements Queue<E> {

    private Node<E> _front, _back;
    private int _size;
    
    public NodeQueue() {
	_size = 0;
	_front = _back = null;
    }
    
    public void enqueue(E element) {
	Node<E> newBack = new Node<E>(element, null);
	if (isEmpty()) _front = _back = newBack;
	else {
	    _back.setNext(newBack);
	    _back = newBack;
	}
	_size++;

    }
    
    public E dequeue() throws EmptyQueueException {
	E ans = front();
	if (size() == 1) _back = null;
	Node<E> temp = _front;
	_front = _front.getNext();
	temp.setNext(null);
	_size--;
	return ans; 
    }
    
    public int size() {
	return _size;
    }
    
    public boolean isEmpty() {
	return _size == 0;
    }
    
    public E front() throws EmptyQueueException {
	if (isEmpty()) throw new EmptyQueueException("Queue is Empty");
	return _front.getValue();
    }
    
    public String toString() {
	if (isEmpty()) return "[]";
	String ans = "[" + front();
	Node curr = _front;
	for (int i = 1; i < size(); i++) {
	    ans += ", " + curr.getNext();
	    curr = curr.getNext();
	}
	return ans + "]";
    }


}
