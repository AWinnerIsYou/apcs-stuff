public class NodeStack<E> implements Stack<E> {
    
    private Node<E> _top;
    private int _size;
    
    public NodeStack() {
	_top = null;
	_size = 0;
    }

    public void push(E element){
	_top = new Node<E>(element, _top);
	_size++;
    }

    public E pop() throws EmptyStackException {
	E ans = top();
	_top = _top.getNext();
	_size--;
	return ans;
    }
	    
    public E top() throws EmptyStackException {
	if (isEmpty())
	    throw new EmptyStackException("Empty Stack");
	return _top.getValue();
    }

    public int size() {
	return _size;
    }

    public boolean isEmpty() {
	return size() == 0;
    }

    
    
}
