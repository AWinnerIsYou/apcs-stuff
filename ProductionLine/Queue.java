public interface Queue<E> {

    /** inserts an element at the rear of the queue
     */
    public void enqueue(E element);
    
    /** returns and removes an element at the front of the queue
     */
    public E dequeue() throws EmptyQueueException;
    
    /** returns the size of the queue
     */
    public int size();
    
    /** returns true if the size is 0
     */
    public boolean isEmpty();

    /**returns the element at the front of the queue
     */
    public E front() throws EmptyQueueException;

}
