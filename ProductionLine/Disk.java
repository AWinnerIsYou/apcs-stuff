public class Disk implements Comparable<Disk>{
    
    private int _radius;

    public Disk(int n) {
	_radius = n;
    }
    
    public int getRadius() {
	return _radius;
    }
    
    public int compareTo(Disk rhs) {
	return getRadius() - rhs.getRadius();
    }


}
