public class ProductionLine {

    private Queue<Disk> _assemblyLineIn;
    private Queue<Tower> _assemblyLineOut;
    private Tower _robotArm;

    public ProductionLine(int nDisks, int maxRadius) {
	_assemblyLineIn = new NodeQueue<Disk>();
	_assemblyLineOut = new NodeQueue<Tower>();
	_robotArm = new Tower();
	for (int i = 0; i < nDisks; i++) {
	    int rad = (int) (Math.random()*maxRadius + 1);
	    _assemblyLineIn.enqueue(new Disk(rad));
	}
    }

    private void unloadRobot() {
	Tower newTower = new Tower();
	while (!_robotArm.isEmpty()) 
	    newTower.push(_robotArm.pop());
	_assemblyLineOut.enqueue(newTower);
    }
    
    public void process() {
	while (!_assemblyLineIn.isEmpty()) {
	    _robotArm.push(_assemblyLineIn.dequeue());
	    while ( (!_assemblyLineIn.isEmpty()) &&
		    (_assemblyLineIn.front().compareTo(_robotArm.top()) > 0))
		_robotArm.push(_assemblyLineIn.dequeue());
	    unloadRobot();
	}
    }

    public static void main(String[] args) {
	ProductionLine P = new ProductionLine(20, 5);
	P.process();
    }
    
}
