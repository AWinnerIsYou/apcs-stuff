public interface Stack<E> {

    /** places element onto the stack
     */
    public void push(E element);

    /** returns and removes the top of the stack
     */
    public E pop() throws EmptyStackException;

    /** returns the top of the stack
     */
    public E top() throws EmptyStackException;

    /** returns the height of the stack
     */
    public int size();
    
    /** returns true if the stack is empty; false otherwise
     */
    public boolean isEmpty();

}
