data: 9, 8, 5, 7, 2

Selection Sort:
Pass #1:	2, 8, 5, 7, 9
Pass #2:	2, 5, 8, 7, 9
Pass #3:	2, 5, 7, 8, 9
Pass #4:	2, 5, 7, 8, 9

Insertion Sort:
Pass #1:	8, 9, 5, 7, 2
Pass #2:	5, 8, 9, 7, 2
Pass #3:	5, 7, 8, 9, 2
Pass #4:	2, 3, 7, 8, 9
