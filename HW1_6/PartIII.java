import java.util.ArrayList;

public class PartIII {
    public static ArrayList<Integer> evens(ArrayList<Integer> L) {
	ArrayList<Integer> temp = new ArrayList<Integer>();
	for (int i = 0; i < L.size(); i++)
	    if (L.get(i) % 2 == 0)
		temp.add(L.get(i));
	return temp;
    }

    public static void main (String[] args) {
	ArrayList<Integer> hi = new ArrayList<Integer>();
	hi.add(1);
	hi.add(6);
	hi.add(4);
	hi.add(13);
	hi.add(15);
	hi.add(42);
	hi.add(33);
	System.out.println(hi);
	System.out.println(evens(hi));
    }
}	
