import wheels.users.*;

public class GreedyBlob extends Blob {

    public GreedyBlob(int x, int y) {
	super(x, y);
    }

    public void mousePressed(java.awt.event.MouseEvent e) {
	super.mousePressed(e);
	this.setSize(this.getWidth() + 20, this.getHeight() + 20);
	this.getRightEye().setSize(this.getRightEye().getWidth() + 6,
					   this.getRightEye().getHeight() + 6);
	this.getLeftEye().setSize(this.getLeftEye().getWidth() + 6,
				   this.getLeftEye().getHeight() + 6);
	this.setLocation(this.getXLocation() - 10, this.getYLocation() - 10);
	this.getRightEye().setLocation(this.getRightEye().getXLocation() - 3,
				       this.getRightEye().getYLocation() - 6);
	this.getLeftEye().setLocation(this.getLeftEye().getXLocation() - 3,
				      this.getLeftEye().getYLocation() - 6);

    }

}
