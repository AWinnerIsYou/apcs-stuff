import wheels.users.*;

public class HappyBlob extends TalkativeBlob {

    public HappyBlob(int x, int y) {
	super(x,y,"I'm so happy!");
    }
}
