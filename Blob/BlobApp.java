import wheels.users.*;

public class BlobApp extends Frame {

    private WinkingBlob _winkingBlob;
    private TalkativeBlob _talkativeBlob;
    private GreedyBlob _greedyBlob;

    public BlobApp() {
	super();
	_winkingBlob = new WinkingBlob(0,100);
	_talkativeBlob = new TalkativeBlob(170,100);
	_greedyBlob = new GreedyBlob(340,100);
    }

    public static void main(String[] args) {
	BlobApp app = new BlobApp();
    }
}
