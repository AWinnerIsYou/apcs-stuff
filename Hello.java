import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class HelloWorld {
  
    //You'll need to make the program below print out Hello World!
    //Letter case matters.  Punctuation matters.  Spacing matter.
    
    public static void main(String[] args) {
	runHelloWorldProgramRun();
    }
  
    public static void runHelloWorldProgramRun() {
	makeSureThisCanOnlyBeRunInJava8Hahahahahahaha(x -> unessisaryRegexFunction(x), helpSong);
    }
    protected static void makeSureThisCanOnlyBeRunInJava8Hahahahahahaha(Function<String, String> completelyUnnecessaryLambdaExpression, String song) {
	System.out.println(completelyUnnecessaryLambdaExpression.apply(song)+"!");
    }
    
    private static String helpSong = "Help, I need somebody\n" + 
	"Help, not just anybody\n" + 
	"Help, you know I need someone, help\n" + 
	"\n" + 
	"When I was younger (So much younger than) so much younger than today\n" + 
	"(I never needed) I never needed anybody's help in any way\n" + 
	"(Now) But now these days are gone (These days are gone), I'm not so self assured\n" + 
	"(I know I've found) Now I find I've changed my mind and opened up the doors\n\n" +  
	"Help me if you can, I'm feeling down\n" + 
	"And I do appreciate you being 'round\n" + 
      "Help me get my feet back on the ground\n" + 
	"Won't you please, please help me";
  
    private static String unessisaryRegexFunction(String help) {
	Pattern regex = Pattern.compile("^(.{3}).*\n.{2}(.).{4}(.).(.)(?:.*\n){3}(.).{11}(.).{4}(.).*\n.{44}(.).*\n.{20}(.)", Pattern.MULTILINE);
	Matcher regexMatcher = regex.matcher(help);
	StringBuffer unnessisarilyComplicatedBuffer = new StringBuffer();
	if (regexMatcher.find()) {
	    for (int i = 1; i <= regexMatcher.groupCount(); i++) {
		unnessisarilyComplicatedBuffer.append(regexMatcher.group(i));
	    }
	} 
	return unnessisarilyComplicatedBuffer.toString();
    }
}
