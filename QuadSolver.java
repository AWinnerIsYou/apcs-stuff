public class QuadSolver {
	public static void main (String [] args) {
		// declare variables
		double r1, r2, a, b, c, discr;

		// initialize variables
		a = Double.parseDouble(args[0]);
		b = Double.parseDouble(args[1]);
		c = Double.parseDouble(args[2]);
		discr = b*b - 4 * a * c;
		r1 = (-b - Math.sqrt(discr))/(2 * a);
		r2 = (-b + Math.sqrt(discr))/(2 * a);

		// output
		System.out.println("roots: " + r1 + ", " + r2);
	}
}
