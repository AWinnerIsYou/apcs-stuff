public class Scoreboard {
    
    private GameEntry _break;
    private int _size;
    
    public Scoreboard() {
	this(10);
    }

    // pre: size > 0
    public Scoreboard(int size) {
	init(size);
    }
    
    private void init(int size) {
	_break = new GameEntry(null,0);
	for (int i = 0; i < size; i++) {
	    GameEntry entry = new GameEntry();
	    if (isEmpty()) {
		_break.setNext(entry);
		entry.setNext(_break);
		_break.setPrevious(entry);
		entry.setPrevious(_break);
	    }
	    else {
		entry.setNext(_break.getNext());
		_break.getNext().setPrevious(entry);
		_break.setNext(entry);
		entry.setPrevious(_break);
	    }
	    _size++;
	}
    }


    // accessor methods
    public int getSize() {
	return _size;
    }

    public GameEntry getTopScore() {
	return _break.getNext();
    }

    public boolean isEmpty() {
	return _size == 0;
    }

    public String toString() {
	String ans = "Rank\tInitials\tScore\n";
	GameEntry curr = getTopScore();
	for (int i = 0; i < _size; i++) {
	    ans += (i+1) + ".\t" + curr + "\n";
	    curr = curr.getNext();
	}
	return ans;
    }
    
    public void initialize(int maxScore, int len) {
	for (int i = 0; i < _size; i++) {
	    GameEntry entry = GameEntry.randomEntry(maxScore,len);
	    //	    System.out.println(entry);
	    if (add(entry))
		System.out.println("added " + entry);
	    else
		System.out.println("did not add " + entry);
	}
	System.out.println();
    }

    
    public boolean add(GameEntry entry){	
	if (entry.compareTo(_break.getPrevious()) < 0) 
	    return false;

	GameEntry entryCopy = new GameEntry(entry.getName(), entry.getScore());
	GameEntry curr = getTopScore();
	while (curr != _break) {
	    if (entryCopy.compareTo(curr) >= 0) {
		//swap names
		entryCopy.setName(curr.setName(entryCopy.getName()));
		//swap scores
		entryCopy.setScore(curr.setScore(entryCopy.getScore()));
	    }
	    curr = curr.getNext();
	}
	return true;
    }
    

    public static void main(String[] args) {
	Scoreboard S = new Scoreboard();
	System.out.println(S);
	S.initialize(1000,3);
	System.out.println(S);
	S.initialize(1000,3);
	System.out.println(S);

	/*
	GameEntry curr = S.getTopScore();
	while (true) {
	    System.out.println(curr);
	    curr = curr.getNext();
	    try {
		Thread.sleep(500); 
	    } catch(InterruptedException ex) {
		Thread.currentThread().interrupt();
	    }
	}
	/*
	System.out.println(S.add(new GameEntry("BAC",-1)));
	
	System.out.println(S);	
	*/
    }
}
