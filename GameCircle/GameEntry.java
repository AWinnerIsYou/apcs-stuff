public class GameEntry implements Comparable<GameEntry> {
    
    private String _name;
    private int _score;
    private GameEntry _previous, _next;
    
    // constructors
    public GameEntry(String name, int score) {
	_name = name;
	_score = score;
	_previous = null;
	_next = null;
    }

    public GameEntry() {
	this("AAA", 0);
    }
    
    
    // accessors
    public String getName() {
	return _name;
    }

    public int getScore() {
	return _score;
    }
    
    public GameEntry getPrevious() {
	return _previous;
    }
    
    public GameEntry getNext() {
	return _next;
    }

    // modifiers
    public String setName(String newName) {
	String ans = getName();
	_name = newName;
	return ans;
    }

    public int setScore(int newScore) {
	int ans = getScore();
	_score = newScore;
	return ans;
    }

    public GameEntry setPrevious(GameEntry n) {
	GameEntry ans = getPrevious();
	_previous = n;
	return n;
    }

    public GameEntry setNext(GameEntry n) {
	GameEntry ans = getNext();
	_next = n;
	return n;
    }

    public int compareTo(GameEntry rhs) {
	if (getScore() == rhs.getScore())
	    return getName().compareTo(rhs.getName());
	return getScore() - rhs.getScore();
    }

    public String toString() {
	return _name + "\t\t" + _score;
    }


    // static methods
    // post: returns a rand integer from [0,max)
    public static int randomScore(int max) {
	return (int)(max*Math.random());
    }

    // post: use only capital letters, returns string of length len
    public static String randomInitials(int len) {
	String ans = "";
	String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (int i = 0; i < len; i++) {
	    int ind = (int) (26*Math.random());
	    ans += alpha.substring(ind, ind+1);
	}
	return ans;
    }

    // post : returns a random GameEntry where 0 <= score < maxScore,
    // _initials.length() = len
    public static GameEntry randomEntry(int maxScore, int len) {
	return new GameEntry(randomInitials(len), randomScore(maxScore));
    }
}

