public class HW39 {
    
    /*
      Part 1
      Write the free method merge(List<Integer> x, List<Integer> y).
      Assume lists x and y are sorted in ascending order, merge returns a sorted
      list by merging the lists x and y.
      The method should not produce any side effects and perform in O(n).
      example: merge([0,1,3,3,10], [2,5,8]) -> [0,1,2,3,3,5,8,10]
    */
    public static List<Integer> merge(List<Integer> x, List<Integer> y) {
	List<Integer> ret = new ArrayList<Integer>();
	int i = 0;
	int j = 0;
	while ((i < x.size()) && (j < y.size())) {
	    if (x.get(i) < y.get(j)) {
		ret.add(x.get(i));
		i++;
	    }
	    else {
		ret.add(y.get(j));
		j++;
	    }
	}

	if (i >= x.size())
	    while (j < y.size()) {
		ret.add(y.get(j));
		j++;
	    }

	if (j >= y.size())
	    while (i < x.size()) {
		ret.add(x.get(i));
		i++;
	    }
	return ret;
    }

    public static void main(String[] args) {
	List<Integer> a = new ArrayList<Integer>();
	a.add(0);
	a.add(1);
	a.add(3);
	a.add(3);
	a.add(10);
	List<Integer> b = new ArrayList<Integer>();
	b.add(2);
	b.add(5);
	b.add(8);
	System.out.println(merge(b,a));
    }
}
	
    
