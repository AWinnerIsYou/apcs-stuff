public class Cult {
    
    private CircleList _circle;
    private int _kth;

    public Cult(int numPeople, int kth) {
	_circle = new CircleList();
	_circle.add(new Node(Integer.toString(numPeople), null)); //first person

	for (int i = numPeople-1; i > 0; i--)
	    _circle.add(new Node(Integer.toString(i), null));
	
	_kth = kth;
    }
    
    public void initiateCult() {
	System.out.println(_circle);
	int originalSize = _circle.size();
	while (_circle.size() != 1) {
	    if (_circle.size() == originalSize)
		for (int i = 0; i < _kth; i++)
		    _circle.advance();
	    else
		for (int i = 0; i < _kth-1; i++)
		    _circle.advance();
	    
	    System.out.println("poisoned: " + _circle.getCursor());
	    _circle.remove();
	}
	System.out.println("survivor: " + _circle.getCursor());
    }
    
    public String toString() {
	return _circle.toString();
    }

    public static void main(String[] args) {
	int M = Integer.parseInt(args[0]);
	int k = Integer.parseInt(args[1]);
	Cult illuminati = new Cult(M,k);
	illuminati.initiateCult();
    }

}
