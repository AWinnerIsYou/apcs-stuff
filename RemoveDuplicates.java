public class RemoveDuplicates {
    
    public static String  removeDuplicates(String w) {
	String ret= "";
	for (int i = 0; i < w.length(); i++)
	    if (ret.indexOf(w.substring(i,i+1)) == -1)
		ret += w.substring(i,i+1);
	return ret;
    }
		    

    public static void main(String[] args) {
	System.out.println(removeDuplicates("abc"));
	System.out.println(removeDuplicates("abbcba"));
    }

}
