public class RandomStat {
    public static void main(String[] args) {
	double n1, n2, n3, n4, n5, avg, max, min;
	n1 = Math.random();
	n2 = Math.random();
	n3 = Math.random();
	n4 = Math.random();
	n5 = Math.random();
	avg = (n1 + n2 + n3 + n4 + n5)/5.0;
	max = Math.max(n1, Math.max(n2, Math.max(n3, Math.max(n4,n5))));
	min = Math.min(n1, Math.min(n2, Math.min(n3, Math.min(n4,n5))));
	System.out.println("average: " + avg);
	System.out.println("max: " + max);
	System.out.println("min: " + min);
    }
}
