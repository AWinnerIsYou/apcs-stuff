public class AddArrays {
    public static void printArray(int[][] a) {
	for (int i = 0; i < a.length; i++) {
	    for (int j = 0; j < a.length; j++) System.out.print(a[i][j] + " ");
	    System.out.println();
	}
    }
    public static void main(String[] args) {
	int a[][] = {{1,2,3},{4,5,6},{7,8,9}};
	int b[][] = {{0,0,0},{4,6,6},{7,8,11}};
	int c[][] = new int[3][3];
	for (int i = 0; i < a.length; i++) {
	    for (int j = 0; j < a.length; j++) {
		c[i][j] = a[i][j] + b[i][j];
	    }
	}
	printArray(c);
    }
}
