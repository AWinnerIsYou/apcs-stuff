public class Average {
    

    public static void main(String[] args) {
	double sum = 0.0;
	int N = 0;

	while (!StdIn.isEmpty()) {
	    sum += StdIn.readDouble();
	    N++;
	}

	System.out.println("avg: " + sum/N);
    }
}
