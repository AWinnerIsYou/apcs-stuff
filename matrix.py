import string, random

ascii = string.ascii_letters + string.digits
colors = ["\033[96m", "\033[95m","\033[94m","\033[93m","\033[92m","\033[91m"]

while True:
    print random.choice(colors) + random.choice(ascii),
