public class BooleanMatrixMult{

    public static void print(boolean[][] a){
	for (int i = 0; i < a.length; i++){
	    for (int j = 0; j < a.length; j++)
		System.out.print(a[i][j] + "\t");
	    System.out.println();
	}

    }

    
    public static void main(String [] args){
	boolean T = true;
	boolean F = false;
	boolean [][] a = {{T,F,T},{F,F,T},{T,T,T}};
	boolean [][] b = {{T,F,F},{F,T,F},{F,F,T}};
	int N = a.length;
	boolean [][] c = new boolean[N][N];


	System.out.println("A");
	print(a);
	System.out.println("B");
	print(b);
	// matrix multiplication: AB = C
        // **********************************
	for (int i = 0; i < N; i++)
	    for (int j = 0; j < N; j++)
	        for (int k = 0; k < N; k++)
                    c[i][j] = (c[i][j])||((a[i][k])&&(b[k][j]));
          
	//***********************************
	System.out.println("AB");
	print(c);
	
    }



}
