public class Feb4 {
    
    // pre: n >= 0 
    public static String withCommas(int n){
	String str = Integer.toString(n);	
	if (str.length() <= 3) return str;

	return withCommas(n/1000) + "," + str.substring(str.length()-3);
    }


    public static void main(String[] args) {
	System.out.println(withCommas(1009005));
    }

}
