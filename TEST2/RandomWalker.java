public class RandomWalker {
    public static void main(String [] args) {
	int x, y, N;
	N = Integer.parseInt(args[0]);
	x = y = 0;
	for (int i = 0; i < N; i++) {
	    double rand = Math.random();
	    if (rand < 0.25) x++;
	    else if (rand < 0.5) x--;
	    else if (rand < 0.75) y++;
	    else y--;
	}
	double dist = Math.sqrt(x^2 + y^2);
	System.out.println(dist);
    }
}
