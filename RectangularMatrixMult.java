public class RectangularMatrixMult{

    public static void print(int[][] a){
	for (int i = 0; i < a.length; i++){
	    for (int j = 0; j < a[0].length; j++)
		System.out.print(a[i][j] + " ");
	    System.out.println();
	}
	
    }
    
    public static void main(String [] args){
	int [][] a = {{1,2,3},{4,5,6}};
	int [][] b = {{7,8},{9,10},{11,12}};
	int N = a.length;
	int M = b.length;
	int [][] c = new int[N][N];
	
	System.out.println("A");
	print(a);
	System.out.println("B");
	print(b);
	// matrix multiplication: AB = C
        // **********************************
	for (int i = 0; i < N; i++)
	    for (int j = 0; j < N; j++)
	        for (int k = 0; k < M; k++)
                    c[i][j] += (a[i][k])*(b[k][j]);          
	//***********************************
	System.out.println("AB");
	print(c);
	
    }
    


}
