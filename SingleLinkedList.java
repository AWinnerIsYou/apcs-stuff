public class SingleLinkedList {

    protected Node _head, _tail;
    protected int _size;

    public SingleLinkedList(){
	_head = _tail = null;
	_size = 0;
    }

    public void addFirst(String value){
	Node t = new Node(value,_head);
	if (_size == 0) _head = _tail = t;
	else   _head = t;
	_size++;
    }

    public void addLast(String value){
	if (_size == 0) addFirst(value);
	else {
	    _tail.setNext(new Node(value, null));
	    _tail = _tail.getNext();
	    ++_size;
	}
    }

    public String removeFirst(){
	if (_size == 0) throw new IllegalStateException();
	String ans = _head.getValue();
	_head = _head.getNext();
	--_size;
	if (_size == 0) _tail = _head;
	return ans;
    }

    public String removeLast(){
	if (_size <= 1) return removeFirst();
	String ans = _tail.getValue();
	Node current = _head;
	while (current.getNext() != _tail) current = current.getNext();
	_tail = current;
	_tail.setNext(null);
	_size--;
	return ans;
    }


    public int size(){
	return _size;
    }

    public String toString(){
	String ans  = "";
	Node current = _head;
	for (int i = 0; i < _size; i++){
	    ans += current.getValue() +  " ";
	    current = current.getNext();

	}
	return ans;
    }

    // ** exercises *************************


    public String getFirst(){
	if ( size() == 0) throw new IllegalStateException();
	return _head.getValue();
    }

    public String getLast(){
	if ( size() == 0) throw new IllegalStateException();
	return _tail.getValue();
    }


    public String get(int i){
	if ( i < 0 || i >= size() ) throw new IllegalStateException();
	Node current = _head;
	for (int j = 0; j < i; j++)
	    current = current.getNext();
	return current.getValue();
    }




    public SingleLinkedList append(SingleLinkedList L){
	SingleLinkedList ans = new SingleLinkedList();
	Node current = _head;
	for (int i = 0; i < size(); i++){
	    ans.addLast(current.getValue());
	    current = current.getNext();
	}
	current = L._head;
	for (int i = 0; i < L.size() ; i++){
	    ans.addLast(current.getValue());
	    current = current.getNext();
	}
	return ans;
    }


    public void reverse(){
	if (_size <= 1) return;
	String val = removeLast();
	reverse();
	addFirst(val);
    }

    public Node search(String key){
	Node current = _head;
	for(int i = 0; i < size(); i++){
	    if (current.getValue().equals(key))
		return current;

	}
	return null;
    }

    public void swap(Node x, Node y){
	if (x == y) return;
	Node current = _head;
	while (current.getNext() != x && current.getNext() != y)
	    current = current.getNext();
	Node current2 = current;
	while (current2.getNext() != x && current2.getNext() != y)
	    current2 = current2.getNext();
	
    }



    public static void main(String [] args){
	SingleLinkedList L = new SingleLinkedList();
	SingleLinkedList L2 = new SingleLinkedList();
	L2.addFirst("Joe");
	L2.addLast("Cal");
	//	System.out.println(L);
	L.addFirst("Bill");
	//System.out.println(L);
	L.addFirst("Mary");
	//System.out.println(L);
	L.addLast("Sam");
	//System.out.println(L);
	//L.reverse();
	System.out.println(L);
	System.out.println(L2);
	System.out.println(L.append(L2));
	/*	while (L.size() > 0){
	    L.removeLast();
	    System.out.println(L);
	}
	*/
    }

}
