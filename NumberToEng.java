
public class NumberToEng {
    
    public static String digitToEng(int n) {
	String[] digit = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
	return digit[n];
    }

    public static String specialTens(int n) {
	String[] foo = {"ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};
	return foo[n % 10];
    }			    

    public static String nonSpecialTens(int n) {
	String[] foo = {"twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"};
	if (n % 10 == 0) return foo[n/10 - 2];
	else return foo[n/10 - 2] + "-" + digitToEng(n%10);
    }

    public static String tensToEng(int n) {
	if (n < 20) return specialTens(n);
	else return nonSpecialTens(n);
    }

    public static String hundToEng(int n) {
	if (n % 100 == 0) return digitToEng(n/100) + " hundred";
	else return digitToEng(n/100) + " hundred " + numToEng(n%100);
    }

    public static String thousToEng(int n) {
	if (n % 1000 == 0) return numToEng(n/1000) + " thousand";
	else return numToEng(n/1000) + " thousand " + numToEng(n%1000);
    }

    public static String numToEng(int n) {
	if (n < 0) return "NO NEGATIVES";	
	if (n < 10) return digitToEng(n);
	if (n < 100) return tensToEng(n); 
	if (n < 1000) return hundToEng(n);
	if (n < 1000000) return thousToEng(n);
	if (n == 1000000) return "one million";
	else return "ONLY UP TO A MILLION";
    }

    public static void main(String[] args) {
	for (int i = 0; i < args.length; i++)
	    System.out.println(numToEng(Integer.parseInt(args[i])));
    }
}
    
