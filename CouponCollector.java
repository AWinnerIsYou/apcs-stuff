public class CouponCollector {
    public static int oneTrial(int N) {
        boolean[] found = new boolean[N];    // found[i] = true if card i has been collected
        int cardcnt = 0;                     // total number of cards collected
        int valcnt = 0;                      // number of distinct cards
	
        // repeatedly choose a random card and check whether it's a new one
        while (valcnt < N) {
            int val = (int) (Math.random() * N);   // random card between 0 and N-1
            cardcnt++;                             // we collected one more card
            if (!found[val]) valcnt++;             // it's a new card type
            found[val] = true;                     // update found[]
        }

        // print the total number of cards collected
        return cardcnt;
    }


    public static double harmonic(int numtypes) {
	double ans = 0.0;
	for (int i = 1; i <= numtypes; i++) {
	    ans += 1.0 / i;
	}
	return ans;

    }
	
    public static void main(String[] args) {
        int numtypes = Integer.parseInt(args[0]);   // number of card types
	int numtrials = Integer.parseInt(args[1]);  // number of trials
	int sum_all_trials = 0;
	for (int i = 0 ; i < numtrials; i++) {
	    sum_all_trials += oneTrial(numtypes);
	}
	System.out.println("Average of all trials " + sum_all_trials/(1.0 * numtrials));
	System.out.println("Expected: " + (numtypes * harmonic(numtypes)));
    }
}
