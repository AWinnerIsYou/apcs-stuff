import java.util.ArrayList;
import java.util.List;

public class DoNow2 {
        
    // pre: list is empty, n > 0
    public static void initialize(List<Integer> list, int n) {
	for (int i = 0; i < n; i++)
	    list.add(i);
    }

    public static void addDoubles(List<Integer> list) {
	for (int i = 0; i < list.size(); i+= 2) {
	    list.add(i+1, list.get(i) *2);
	}
    }

    public static void removeEvens(List<Integer> list) {
	for (int i = 0; i < list.size(); i++)
	    if (list.get(i) % 2 == 0) {
		list.remove(i);
		i--;
	    }
	
    }

    public static void main(String[] args) {
	List<Integer> L =new ArrayList<Integer>();
	System.out.println(L); // []
	initialize(L,5);       // [0,1,2,3,4]
	System.out.println(L);
	addDoubles(L);         // [0,0,1,2,2,4,3,6,4,8]
	System.out.println(L);
	removeEvens(L);
	System.out.println(L);
	

    }

}
