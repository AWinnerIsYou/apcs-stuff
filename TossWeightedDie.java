public class TossWeightedDie {
    public static void main(String[] args) {
	int numTimes = Integer.parseInt(args[0]);
	while (numTimes != 0) {
	    double rand = Math.random()*100;
	    if (rand < 25.0) System.out.println(4);
	    else if (rand < 50.0) System.out.println(3);
	    else if (rand < 65.0) System.out.println(5);
	    else if (rand < 80.0) System.out.println(2);
	    else if (rand < 90.0) System.out.println(6);
	    else System.out.println(1);
	    numTimes--;
	}
    }
}