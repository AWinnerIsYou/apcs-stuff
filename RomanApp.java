import java.util.ArrayList;

public class RomanApp {
    
    /*
      pre: size > 0, lower < upper
      post: returns a list with size elements
      each element is a random roman Object whose value is in [ lower, upper)
    */    
    public static ArrayList<Roman> populate(int size, int lower, int upper) {
	ArrayList<Roman> ans = new ArrayList<Roman>();
	for (int i = 0; i < size; i++) {
	    int rand = (int)(Math.random()*(upper-lower) + lower);
	    ans.add(new Roman(rand));
	}
	return ans;

    }
    
    public static Roman min(ArrayList<Roman> L) {
	Roman curr = L.get(0);
	for (int i = 1; i < L.size(); i++)
	    if (L.get(i).compareTo(curr) < 0)
		curr = L.get(i);
	return curr;
    }
      

    public static Roman sum(ArrayList<Roman> L) {
	int ans = 0;
	for (Roman roman : L)
	    ans += roman.intValue();
	return new Roman(ans);
    }

    public static ArrayList<Roman> filterEvens(ArrayList<Roman> L) {
	ArrayList<Roman> ans= new ArrayList<Roman>();
	for (Roman roman : L) 
	    if (roman.intValue() % 2 == 0)
		ans.add(roman);
	return ans;
    }


    public static void mapAdd(int n, ArrayList<Roman> L) {
	for (int i = 0; i < L.size(); i++)
	    L.set(i, new Roman(L.get(i).intValue() + n));
    }

    public static void main(String[] args) {
	ArrayList<Roman> romans = populate(8,2,100);
	System.out.println(romans);
	System.out.println(filterEvens(romans));
	
    }

}
