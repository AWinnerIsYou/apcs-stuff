public class DNode<E>{

    private E _value;
    private DNode<E> _previous, _next;


    public DNode(E value, DNode<E> previous, DNode<E> next){
	_value = value;
	_previous = previous;
	_next = next;
    }

    // accessor methods
    public E getValue(){
	return _value;
    }

    public DNode<E> getNext(){
	return _next;
    }

    public DNode<E> getPrevious(){
	return _previous;
    }

    // modifier methods
    public E setValue(E newValue){
        E ans = getValue();
	_value = newValue;
	return ans;
    }

    public DNode<E> setNext(DNode<E> newNext){
	DNode<E> ans = getNext();
	_next = newNext;
	return ans;
    }

    public DNode<E> setPrevious(DNode<E> newPrevious){
	DNode<E> ans = getPrevious();
	_previous = newPrevious;
	return ans;
    }

    public String toString(){
	return _value.toString();
    }



}
