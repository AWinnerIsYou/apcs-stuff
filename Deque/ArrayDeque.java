import java.util.Arrays;

public class ArrayDeque<E> implements Deque<E> {

    private E[] _deque;
    private int _head, _tail, _size;

    private static final int CAPACITY = 1000;
    
    public ArrayDeque() {
	this(CAPACITY);
    }

    public ArrayDeque(int capacity) {
	_deque = (E[]) new Object[capacity+1]; //compiler warning
	_size = _head = 0;
	_tail = 1;
    }

    public int size() {
	return _size;
    }

    public boolean isEmpty() {
	return size() == 0;
    }
 
    
    public E getFirst() throws EmptyDequeException {
	if (isEmpty()) throw new EmptyDequeException("Deque is Empty");
	return _deque[floorMod(_head+1, _deque.length)];
    }
 
   public E getLast() throws EmptyDequeException {
	if (isEmpty()) throw new EmptyDequeException("Deque is Empty");
	return _deque[floorMod(_tail-1, _deque.length)];
    }
    
    public void addFirst(E val) throws FullDequeException{
	if (_head == _tail) throw new FullDequeException("Deque is Full");
	_deque[_head] = val;
	_head = floorMod(_head-1, _deque.length);
	_size++;    
    }

    public void addLast(E val) throws FullDequeException {
	if (_head == _tail) throw new FullDequeException("Deque is Full");
	_deque[_tail] = val;
	_tail = floorMod(_tail+1, _deque.length);
	_size++;
    }
    
   
    public E removeFirst() throws EmptyDequeException {
	if (isEmpty()) throw new EmptyDequeException("Deque is Empty");
	E ans = getFirst();
	int newHead = floorMod(_head+1,_deque.length);
	_deque[newHead] = null;
	_head = newHead;
	return ans;
    }

    public E removeLast()  throws EmptyDequeException{
	if (isEmpty()) throw new EmptyDequeException("Deque is Empty");
	E ans = getLast();
	int newTail = floorMod(_tail-1,_deque.length);
	_deque[newTail] = null;
	_tail = newTail;
	return ans;
    }

    public E pollFirst() {
	if (isEmpty()) return null;
	return removeFirst();
    }

    public E pollLast() {
	if (isEmpty()) return null;
	return removeLast();
    }

    public E peekFirst() {
	if (isEmpty()) return null;
	return getFirst();
    }

    public E peekLast() {
	if (isEmpty()) return null;
	return getLast();
    }

    public boolean offerFirst(E val) {	
	addFirst(val);
	return true;
    }

    public boolean offerLast(E val) {
	addLast(val);
	return true;
    }


    private static int floorMod(double n, double d) {
	return (int)(n - d*Math.floor(n / d));
    }

    public void print() {
	System.out.println(Arrays.toString(_deque));
    }

    public static void main(String[] args) {
	ArrayDeque<Integer> D = new ArrayDeque<Integer>(5);
	D.addFirst(1);
	D.addFirst(2);
	D.addLast(5);
	D.removeFirst();
	D.addLast(7);
	D.addLast(8);
	D.addFirst(3);
	D.removeLast();
	D.addFirst(9);
	D.print();
	
    }
}
