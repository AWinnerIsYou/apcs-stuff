public class DLinkedList<E>{
    
    private int _size;
    private DNode<E> _header, _trailer; // refer to Dummy Nodes

    // constructor
    public DLinkedList(){
	_size = 0;
	_header = new DNode<E>(null, null, null);
	_trailer = new DNode<E>(null, _header, null);
	_header.setNext(_trailer);
    }

 // postcondition : returns the size of the list
    public int size(){
	return _size;
    }


   // postcondition: return true if the list is empty false otherwise
    public boolean isEmpty(){
	return size() == 0;
    }



   // postcondition: returns the first node in the list
   // throws an IllegalStateException if the list is empty.
    public DNode<E> getFirst(){
	if (isEmpty()) throw new IllegalStateException();
	return _header.getNext(); 
    }


   // postcondition: returns the last node in the list
   // throws an IllegalStateException if the list is empty
    public DNode<E> getLast(){
	if (isEmpty()) throw new IllegalStateException();
	return _trailer.getPrevious();
    }
    // returns the node before current
    // throws Illegal Argument Exception if current == _header
    public DNode<E> getPrevious(DNode<E> current){
	if (current == _header) 
	    throw new IllegalArgumentException("cannot move back of header");
	return current.getPrevious();
    }

    // returns the node after current
    // throws Illegal Arguement Exception if current == _trailer
    public DNode<E> getNext(DNode<E> current){
	if (current == _trailer)
	    throw new IllegalArgumentException("cannot move forward of trailer");
	return current.getNext();
    }
    // precondition : b refers to a node in the list
    //                Throws an exception if b refers to the _header
    // postcondtion: inserts node a before node b
    public void addBefore(DNode<E> b, DNode<E> a){
	if (b == _header) throw new IllegalArgumentException("cannot insert before header");
	a.setNext(b);
	a.setPrevious(getPrevious(b));
	b.setPrevious(a);
	a.getPrevious().setNext(a);
	_size++;
    }

    public void addLast(DNode<E> x){
	addBefore(_trailer,x);
    }

    public void addLast(E value){
	addLast(new DNode<E>(value,null,null));
    }

    public String toString(){
	String ans = "[";
	DNode current = _header.getNext();
	while (current != _trailer){
	    ans += current + ", ";
	    current = current.getNext();
	}
	int len = ans.length();
	ans = ans.substring(0,len - 2);
	ans += "]";
	return ans;
    }
    //*******************************************************************
    // inserts node b after node a
    // throw exception if a is the trailer node
    public void addAfter(DNode<E> a, DNode<E> b){
	if ( a == _trailer)
	    throw new IllegalArgumentException();
	b.setNext(a.getNext());
	b.setPrevious(a);
	a.setNext(b);
	b.getNext().setPrevious(b);
	_size++;
    }

    public void addFirst(DNode<E> current){
        addAfter(_header,current);
    }

    public void addFirst(E value){
        addFirst(new DNode<E>(value,null,null));
    }
    // O(n)
    //*******************************************************************
    // postcondition: returns the ith String in the list.
    //                get(0) is the first string
    //                get(size() - 1) is the last string
    //                Throw an exception if index < 0 || index >= size()
    public E  get(int i){
	if (i < 0 || i >= size())
	    throw new IndexOutOfBoundsException();
	DNode<E> t;
	if (i < size() / 2){
	    t = getFirst();
	    for (int a = 0 ; a < i; a++)
		t =  t.getNext();
	}
	else{
	    t = getLast();
	    for (int a = size() - 1; a > i; a--)
		t = t.getPrevious();
	}
	return t.getValue(); 
    }

    // post: throws Exception if v points to header or trailer
    public void remove(DNode<E> v) {
	if (v == _header || v == _trailer) 
	    throw new IllegalStateException();
	getPrevious(v).setNext(getNext(v));
	getNext(v).setPrevious(getPrevious(v));
	v.setNext(null);
	v.setPrevious(null);
	_size--;
    }
    
    public E removeFirst() {
	E ans = getFirst().getValue();
	remove(getFirst());
	return ans;
    }
    
    public DNode<E> middleNode() {
	if (isEmpty()) throw new IllegalStateException();
	DNode<E> a = getFirst();
	DNode<E> b = getLast();
	while (true) {
	    if (a == b) return b;
	    if (b.getPrevious() == a) return a;
	    a = getNext(a);
	    b = getPrevious(b);
	}
	
    }
    
    public void swap(DNode<E> a, DNode<E> b) {  
	DNode<E> aPrev = getPrevious(a);
	DNode<E> aNext = getNext(a);
	DNode<E> bPrev = getPrevious(b);
	DNode<E> bNext = getNext(b);  
	a.setPrevious(bPrev);
	a.setNext(bNext);

	b.setPrevious(aPrev);
	b.setNext(aNext);

	bPrev.setNext(a);
	bNext.setPrevious(a);
	
	aPrev.setNext(b);
	aNext.setPrevious(a);
    }

    public boolean hasPrevious(DNode<E> v){
	return v != _header;
    }



    public boolean hasNext(DNode<E> v){
	return v != _trailer;
    }
    

    public static void main(String [] args){
	DLinkedList<String> L = new DLinkedList<String>();
	
	L.addLast("3");
	L.addLast("4");
	L.addFirst("2");
	L.addFirst("1");
	L.addLast("5");
	
	System.out.println(L); // [Abe, Carol, Jane, Mark]
	System.out.println();

	
	System.out.println("f: " + L.getFirst());
	System.out.println("l: " + L.getLast());
	System.out.println("m: " + L.middleNode());

	// O(n*n)
	/*
	for(int i = 0; i < L.size(); i++){
	    System.out.println(L.get(i));
	}
	*/
	
    }

}
