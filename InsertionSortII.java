import java.util.ArrayList;

public class InsertionSortII {

    // returns List with integers from 0 to n in random order
    public static ArrayList<String> randomList(int n) {
	ArrayList<String> ans = new ArrayList<String>();
	for (int i = 0; i < n; i++) {
	    ans.add(""+i);
	}
	for (int j = 0; j < n; j++) {
	    int newInd = (int) (Math.random()*(n));
	    ans.set(j,ans.set(newInd,ans.get(j)));
	}
	return ans;
    }

    // starting from first
    public static DLinkedList transfer(ArrayList<String> L) {
	DLinkedList ans = new DLinkedList();
	for (int i = 0; i < L.size(); i++)
	    ans.addLast(L.get(i));
	return ans;
    }


    public static DLinkedList transferII(ArrayList<String> L) {
	DLinkedList ans = new DLinkedList();
	for (int i = L.size()-1; i >= 0; i--)
	    ans.addFirst(L.get(i));
	return ans;
    }

    public static void sort(DLinkedList L) {
	System.out.println("shuffled arraylist: " + L);
	int N = L.size();
	DNode pivot = L.getFirst().getNext();
	DNode end = pivot.getNext();
	
	for (int i = 1; i < N; i++) {
	    System.out.println("pivot: " + pivot);
	    System.out.println("end: " + end);
	    DNode compTo = pivot.getPrevious();
	    while (true) {
		if (pivot.getValue().compareTo(compTo.getValue()) > 0) {
		    System.out.println("insert after: " + compTo);
		    L.remove(pivot);
		    L.addAfter(compTo, pivot);
		    break;
		}
		if (!L.hasPrevious(compTo.getPrevious())) {
		    System.out.println("find insertion point : " + compTo);
		    System.out.println("insert after: null");
		    L.remove(pivot);
		    L.addFirst(pivot);
		    break;
		}
		
		else {
		    System.out.println("find insertion point : " + compTo);
		    compTo = compTo.getPrevious();
		}


	    }
	    
	    
	    System.out.println("sorting: " + L);
	    pivot = end;
	    end = pivot.getNext();
	}
	
    }
    
    public static void main(String[] args) {
	int size = Integer.parseInt(args[0]);
	ArrayList<String> list1 = randomList(size);
	DLinkedList list2 = transfer(list1);
	sort(list2);
    }

}

