import wheels.users.*;

public class RockGame extends Frame {
    
    private Rock _rock1;
    private Rock _rock2;
    private Rock _rock3;

    public RockGame() {
	super();
	_rock1 = new Rock(rx(),ry());
	_rock2 = new Rock(rx(),ry());
	_rock3 = new GoldRock(rx(),ry());
    }

    public static int rx() {
	return (int) (Math.random() * 680);
    }

    public static int ry() {
	return (int) (Math.random() * 480);
    }

    public static void main(String[] args) {
	RockGame game = new RockGame();
    }
}

class Rock extends Rectangle {

    public Rock(int x, int y) {
        super(x,y);
        this.setSize(20,20);
	this.setColor(java.awt.Color.LIGHT_GRAY);
    }

}

class GoldRock extends Rock {

    public GoldRock(int x, int y) {
	super(x,y);
    }

    public void mousePressed(java.awt.event.MouseEvent e) {
	this.setColor(new java.awt.Color(218,165,32));
    }

    public void mouseReleased(java.awt.event.MouseEvent e) {
	this.setColor(java.awt.Color.LIGHT_GRAY);
    }
}

