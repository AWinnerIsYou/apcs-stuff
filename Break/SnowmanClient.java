import wheels.users.*;

public class SnowmanClient extends Frame {
    
    private Snowman _snowman;
    private MeltingSnowman _meltingSnowman;
    
    public SnowmanClient() {
	super();
	_snowman = new Snowman(0,0);
	_meltingSnowman = new MeltingSnowman(200,0);
    }

    public static void main(String[] args) {
	SnowmanClient _client = new SnowmanClient();
    }
}

class Snowman extends Rectangle {

    private Ellipse _head, _middleBody, _lowerBody;
    private Rectangle _upperHat, _lowerHat;

    public Snowman(int x, int y) {
	super(x,y);
	
	_head = new Ellipse(x+40,y+50);
	_head.setFrameColor(java.awt.Color.BLACK);
	_head.setFillColor(java.awt.Color.WHITE);
	_head.setSize(70,70);

	_middleBody = new Ellipse(x+35,y+120);
	_middleBody.setFrameColor(java.awt.Color.BLACK);
	_middleBody.setFillColor(java.awt.Color.WHITE);
	_middleBody.setSize(80,80);

	_lowerBody = new Ellipse(x+25,y+200);
	_lowerBody.setFrameColor(java.awt.Color.BLACK);
	_lowerBody.setFillColor(java.awt.Color.WHITE);
	_lowerBody.setSize(100,100);

	_upperHat = new Rectangle(x+55,y+0);
	_upperHat.setColor(java.awt.Color.BLACK);
	_upperHat.setSize(40,40);

	_lowerHat = new Rectangle(x+30,y+40);
	_lowerHat.setColor(java.awt.Color.BLACK);
	_lowerHat.setSize(90,10);

	this.setFrameColor(new java.awt.Color(0,0,0,0));
	this.setFillColor(new java.awt.Color(0,0,0,0));
	this.setSize(150,300);
	
    }

    public Ellipse getHead() {
	return _head;
    }

    public Ellipse getMiddleBody() {
	return _middleBody;
    }

    public Ellipse getLowerBody() {
	return _lowerBody;
    }

    public Rectangle getUpperHat() {
	return _upperHat;
    }

    public Rectangle getLowerHat() {
	return _lowerHat;
    }
}

class MeltingSnowman extends Snowman {
    
    public MeltingSnowman(int x, int y) {
	super(x,y);
    }

    public void mousePressed(java.awt.event.MouseEvent e){
	this.getUpperHat().setColor(java.awt.Color.RED);
    }
}

