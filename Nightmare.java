public class Nightmare {
	public static void main (String [] args) {
		int a = Integer.MAX_VALUE;
		System.out.println("max: " + a);
		System.out.println("min: " + Integer.MIN_VALUE);
		System.out.println();
		System.out.println("a + 1 = " + (a + 1));
		System.out.println("2 - a = " + (2 - a));
		System.out.println("-2 - a = " + (-2 - a));
		System.out.println("2 * a = " + (2*a));
		System.out.println("4 * a = " + (4*a));
	}
}
