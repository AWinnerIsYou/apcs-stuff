public class MonteHall {
    public static void main(String[] args) {
	int N = Integer.parseInt(args[0]);
	System.out.println("From " + args[0] + " game(s), not switching resulted in " + countNoSwitchWins(N) + " win(s).");
	System.out.println("From " + args[0] + " game(s),     switching resulted in " + countSwitchWins(N) + " win(s).");
    }
    public static boolean oneGameWithoutSwitch() {
	boolean a, b, c, choice;
	a = b = c = false;
	
	//set door w/ prize(true). rest are left with fake prize(false)
	double rand = Math.random();
	if (rand < (1.0 / 3.0)) a = true;
	else if (rand < (2.0 / 3.0)) b = true;
	else c = true;
	
	//set choice
	rand = Math.random();
	if (rand < (1.0 / 3.0)) choice = a;
	else if (rand < (2.0 / 3.0)) choice = b;
	else choice = c;

	return choice;
    }

    public static int countNoSwitchWins(int N) {
	int count = 0;
	for (int i = 0; i < N; i++) {
	    if (oneGameWithoutSwitch()) count++;
	}
	return count;
    }

    public static boolean oneGameWithSwitch() {
	//same as oneGameWithoutSwitch()
	boolean a, b, c, choice; 
	String choiceLetter;
	a = b = c = false;
	double rand = Math.random();
	if (rand < (1.0 / 3.0)) a = true;
	else if (rand < (2.0 / 3.0)) b = true;
	else c = true;
	rand = Math.random();
	if (rand < (1.0 / 3.0)) {choice = a; choiceLetter = "a";}
	else if (rand < (2.0 / 3.0)) {choice = b; choiceLetter = "b";}
	else {choice = c; choiceLetter = "c";}
	
	/*
	//the switch could be the following:
	rand = Math.random();	
	if (choiceLetter == "a") {
	  if (choice == true) choice = false; 
	  //because you selected the right door
	  else choice = true; 
	  // since the host shows you the door with the fake price, and your current choice is the other fake prize, switching results in a win.
	  // so basically this if statement simplifies to choice = !choice...
	}
	else if (choiceLetter == "b") choice = !choice;
	else if (choiceLetter == "c") choice = !choice;
	//...similar too these two if statements.
	*/
	choice = !choice; //simplified switch
	return choice;
    }

    public static int countSwitchWins(int N) {
	int count = 0;
	for (int i = 0; i < N; i++) {
	    if (oneGameWithSwitch()) count++;
	}
	return count;
    }
}
