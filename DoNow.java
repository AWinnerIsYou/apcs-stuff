import java.util.ArrayList;
public class DoNow {

    /*
    //version 1: DOES NOT USE GENERICS: UNSAFE
    public static void populate(ArrayList L) {
	L.add(5);
	L.add(10);
	L.add(1);
	L.add(0);
	L.add("abe");
    }
    */

    /*    
    //version 2: USE GENERICS: SAFE
    //DOES NOT COMPILE
    public static void populate(ArrayList<Comparable> L) {
	L.add(5);
	L.add(10);
	L.add(1);
	L.add(0);
	L.add("abe");
    }
    */

    
    //version 3: USE GENERICS: SAFE
    public static void populate(ArrayList<Integer> L) {
	L.add(5);
	L.add(10);
	L.add(1);
	L.add(0);
	//L.add("abe");
    }
    
    /*
    // max V1: UNSAFE
    public static Comparable max(ArrayList L){
	Comparable temp = (Comparable) L.get(0);
	for (int i = 1; i < L.size(); i++)
	    if (((Comparable) L.get(i)).compareTo(temp) > 0)
		temp = (Comparable) L.get(i);
	return temp;
    }
    */

    //max V2: SAFE
    public static Integer max2(ArrayList<Integer> L){
	Integer temp = L.get(0);
	for (int i = 1; i < L.size(); i++)
	    if (L.get(i) > temp)
		temp = L.get(i);
	return temp;
    }

    public static void reverse (ArrayList<Integer> L) {
	for (int i = 0; i < L.size()/2; i++)
	    L.set(L.size()-1-i, 
		  L.set(i, L.get(L.size()-i-1)));
    }


    public static void main(String[] args) {
	ArrayList<Integer> L = new ArrayList<Integer>();
	populate(L);
	reverse(L);
	System.out.println(L); // [5, 10, 1, 0]
	System.out.println(max2(L));
    }
}
							  
