public class CaesarCipher{
   

    // precondition: plainText is a word to be encrypted
    // postcondition: returns an encrypted form of the plainText
    // by shifting each character 3 index positions such that
    // i -> i+3
    // example:  "ape" -> "dsh"
    //           "zoo" -> "crr"
    //           "java"-> "mdyd"
    public static String encrypt(String plainText){
	return encrypt(plainText,3);
    }

    // precondition: shift >= 0
    // postcondition: returns an encrypted word by shifting
    // each character shift index postions. Generalizes
    // the encrypt(String) method.
    public static String encrypt(String plainText,int shift){
	int newShift;
	if (shift >= 0) newShift = shift; //positive shift
	else newShift = 26 + shift;       //negative shift
	String ret = "";
	String alf = "abcdefghijklmnopqrstuvwxyz";
	for (int i = 0; i < plainText.length(); i++) {
	    int newInd = (alf.indexOf(plainText.substring(i,i+1)) + newShift) % 26;
	    ret += alf.substring(newInd, newInd+1);
	}
	return ret;
    }

    public static void main(String [] args){
	String plainText = args[0];
	int shift = Integer.parseInt(args[1]);
	String cipherText = encrypt(plainText, shift);
	System.out.println(cipherText);
    }



}
