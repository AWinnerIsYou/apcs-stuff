public class ArrayIO{

    public static void printArray(Object [] x){
	int N = x.length;
	for (int i = 0; i < N; i++)
	    System.out.print(x[i] + " ");
	System.out.println();
    }

    public static void printArray(Object [][] x){
	int N = x.length;
	for (int i = 0; i < N; i++)
	    printArray(x[i]);
    }

   // precondition: 0 < n
   // postcondition: returns an array of integers
   //                {0,1,2,...,n-2,n-1}
   //                intArray(5) -> {0,1,2,3,4,5}
    public static Integer[] intArray(int n) {
	Integer[] ret = new Integer[n];
	for (int i = 0; i < n; i++)
	    ret[i] = i;
	return ret;
    }



    public static void main(String [] args){
	Integer[] a = {45,16,9,315};
	printArray(a);

	System.out.println();
	
	String[][] b = {{"hello","world"},{"goodbye","earth"}};
	printArray(b);
    }

}
