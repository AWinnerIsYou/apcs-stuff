public class Sorts{

    public static void shuffle(Object [] x){
	int N = x.length;
	for(int i = 0; i < N; i++){
	    int r = (int)(Math.random() * (N-i)) + i;
	    Object t = x[i];
	    x[i] = x[r];
	    x[r] = t;
	}
    }

    // precondition:  x != null
    // postcondition: returns true if x is empty or is sorted
    //                in ascending order; false otherwise.
    // isSorted({1,2,2,3,4}) -> true
    // isSorted({"a","c","b"}) -> false
    // isSorted({0.2, 1.4, 1.0}) -> false
    
    public static boolean isSorted(Comparable [] x){
	for (int i = 0; i < x.length - 1; i++){
	    if (x[i].compareTo(x[i+1]) > 0)
		return false;
	}
	return true;
    }
    
    // precondition : x != null
    // postcondtion : sorts the array in ascending order by
    //                shuffling the array until it is sorted.
    //               bogoSort({1,5,3}) -> {1,3,5}
    //               returns number of shuffles it took to sort the array.
    
    public static int bogoSort(Comparable [] x){
	int ret = 0;
	while (!isSorted(x)) {
	    shuffle(x);
	    ret += 1;
	}
	return ret;
    }
    

    public static void bubbleSort(Comparable[] x) {
	for (int i = 0; i < x.length-1; i++) {
	    for (int j = 0; j < x.length - i - 1; j++) {
		if (x[j+1].compareTo(x[j]) < 0) {
		    Comparable temp = x[j];
		    x[j] = x[j+1];
		    x[j+1] = temp;
		}
	    }
	}
       	
    }
    
    public static void bubbleSortModified(Comparable[] x) {
	int swaps = 0;
	for (int i = 0; i < x.length-1; i++) {
	    for (int j = 0; j < x.length - i - 1; j++) {
		if (x[j+1].compareTo(x[j]) < 0) {
		    Comparable temp = x[j];
		    x[j] = x[j+1];
		    x[j+1] = temp;
		    swaps++;
		}
	    }
	    if (swaps == 0) break;
	    else swaps = 0;
	}
       	
    }

    public static void selectionSort(Comparable[] x) {
	// passes through elements x[0] to x[x.length-1]
	for (int i = 0; i < x.length-1; i++) {
	    int minPos = i; // current index with least value
	    // go through all elements x[j] after x[i]
	    for (int j = i + 1; j < x.length; j++)
		// update minPos if a smaller element x[j] is found
		if (x[minPos].compareTo(x[j]) > 0) minPos = j;
	    // if minPos has changed, swap x[i] and x[minPos]
	    if (minPos != i) {
		Comparable temp = x[i];
		x[i] = x[minPos];
		x[minPos] = temp;
	    }
	    // otherwise don't swap (since that would swap x[i] with itself)
	}
	
    }
    
    public static void main (String[] args) {
	Integer[] a = {4,7,1,10};
	System.out.print("Bogo Sort Test...");
	bogoSort(a);
	System.out.println(isSorted(a));

	Integer[] b = {46,38,1,59,25,79,7,90,94,82,54,98,12,19,89};
	System.out.print("Bubble Sort Test...");
	bubbleSort(b);
	System.out.println(isSorted(b));

	Integer[] c = {46,38,1,59,25,79,7,90,94,82,54,98,12,19,89};
	System.out.print("Selection Sort Test...");
	selectionSort(b);
	System.out.println(isSorted(b));
    }
}
