import java.util.Arrays;
public class MusicShuffle {
    public static void main(String[] args) {
	int N = Integer.parseInt(args[0]);
	int[] songs = new int[N];
	for (int i = 0; i < N; i++) songs[i] = i;  //assigns number to each song
	
	//shuffle songs
	for (int i = 0; i < N; i++) {
	    int randIndex = i + (int)(Math.random() * (N-i));
	    int temp = songs[randIndex];
	    songs[randIndex] = songs[i];
	    songs[i] = temp;
	}
	
	System.out.println(Arrays.toString(songs));

	int countSequential = 0;
	//count sequential songs
	for (int i = 0; i < N - 1; i++) {
	    if (songs[i+1] == songs[i] + 1) countSequential += 1;
	}

	System.out.println("There are " + countSequential + " sequential pairs of songs");
	
	
    }
}
