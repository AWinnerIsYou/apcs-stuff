public class PrimeFactors {
    public static void primeFactorsHelper(int n) {
	String ans = "prime factors of " + n + ": ";
	int curr = n;
	int i = 2;
	while (curr != 1) {
	    if (curr % i == 0) {
		ans += i + " ";
		curr /= i;
	    }
	    else i++;
	}
	System.out.println(ans);
    }
    public static void primeFactors(int a, int b) {
	for (int i = a; i < b; i ++) {
	    primeFactorsHelper(i);
	}
    }
    public static void main(String[] args) {
	primeFactors(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }
}
