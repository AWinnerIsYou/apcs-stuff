public class TenHellos{
    
    /* 1st Hello
       2nd Hello
       .
       .
       .
       10th Hello */

    public static void main(String[] args) {
	int i = 4;
	System.out.println("1st Hello\n2nd Hello\n3rd Hello");
	while (i <= 10) {
	    System.out.println(i + "th " + "Hello");
	    i++;
	}
    }
    
}
