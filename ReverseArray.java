public class ReverseArray {
    public static void printArray(double[] a) {
	System.out.print("[");
	for (int i = 0; i < a.length - 1; i++) System.out.print(a[i] + ", ");
	System.out.print(a[a.length-1] + "]\n");
    }
    public static void main(String[] args) {
	//random length
	int N = (int)Math.floor(Math.random() * 10 + 1);
	//new array
	double[] a = new double[N];
	//assign values
	for (int i = 0; i < a.length; i++) a[i] = Math.random();
	//print array
	printArray(a);
	//reverse
	for (int i = 0; i < N/2; i++) {
	    double temp = a[N-i-1];
	    a[N-i-1] = a[i];
	    a[i] = temp;
	}
	//reprint
	printArray(a);
    }
}