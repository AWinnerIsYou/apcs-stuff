public class Permutations {


    //pre: str != null;
    public static void permutations(String str) {
	ph(str,"");
    }

    //helper
    private static void ph(String str, String ans) {
	if (str.length() == 0)
	    System.out.println(ans+str);
	else
	    for (int i = 0 ; i < str.length(); i++)
		ph(str.substring(0,i) + str.substring(i+1), ans + str.substring(i, i+1));
    }

    
    public static void main(String[] args) {
	permutations(args[0]);
    }

}
