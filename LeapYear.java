public class LeapYear {
    public static void main(String[] args) {
	//declare variables
	boolean ans;
	int year = Integer.parseInt(args[0]);

	//initialize variables
	ans = (year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0));
	
	//output
	System.out.println("Is a leap year: " + ans);
    }
}