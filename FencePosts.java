public class FencePosts {
    public static void main(String[] args) {
	int num = Integer.parseInt(args[0]);
	String ans = "|";
	while (num != 1) {
	    ans += "---|";
	    num--;
	}
	System.out.println(ans);
    }
}