public class TruthTable {
    public static void main(String[] args) {	
	System.out.println("A        B        A&&B     A||B     !A       !(A&&B)  !A||!B");
	System.out.println("============================================================");
	System.out.println("true     true     true     true     false    false    false");
	System.out.println("true     false    false    true     false    true     true");
	System.out.println("false    true     false    true     true     true     true");
	System.out.println("false    false    false    false    true     true     true");
    }
}