public class MyMath {

    public static int abs(int x) {
	if (x < 0) return -x;
	return x;
    }
    public static double abs(double x) {
	if (x < 0) return -x;
	return x;
    }
    
    public static double sqrt(double x) {
	if (x < 0) return Double.NaN;
	double guess = 1.0;
	while (abs(guess - x/guess) > 1e-15*guess ) {
	    guess = (guess + x / guess) / 2.0;
	}
	return guess;
    }

    public static boolean isOdd(int n) {
	return n % 2 == 1;
    }

    public static boolean isEven(int n) {
	return !isOdd(n);
    }

    public static boolean isPrime(int n) {
	if (n < 2) return false;
	for (int d = 2; d <= n/d; d++)
	    if (n % d == 0) return false;
	return true;
    }

    // precondition: n >= 1
    // postcondition 2^(log n) <= n
    public static int log(int n) {
	/*
	int exp = 0;
	while (true) {
	    if (n < Math.pow(2,exp)) 
		return exp-1;
	    exp++;
	}
	*/
	
	int ans = 0;
	while (n != 1) {
	    ans++;
	    n /= 2;
	}
	return ans;
	

    }

    // pre: a and b are positive integers
    public static int gcd(int a, int b) {
	if (b == 0)
	    return a;
	return gcd(b, a % b);
    }
    

    public static void main(String[] args) {
	
	int N = args.length;
	int[] a = new int[N];
	
	//args from command line
	for (int i = 0; i < N; i++)
	    a[i] = Integer.parseInt(args[i]);
	
	//test functions static methods
	for (int i = 0; i< N ; i++) {
	    int x = log(a[i]); 
	    System.out.println("log(" + a[i] + ") = " + x);	    
	}
	

    }
    
}
/*

*/
