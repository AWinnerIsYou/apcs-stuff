public class Rational implements Comparable{

    // instance variables
    private int _n, _d;

    // constructor
    public Rational(int n, int d) {
	if (d == 0)
	    throw new ArithmeticException("denominator can't be zero");
	if (d < 0) {
	    n = -n;
	    d = -d;
	}
	
	int gcd = MyMath.gcd(Math.abs(n), d); //O(logN)
	if (gcd == 0) gcd = 1;
	_n = n/gcd;
	_d = d/gcd;
    }
    
    public Rational(Rational r) {
	this(r._n, r._d);
    }
    
    // O(logN)
    public Rational add(Rational other) {
	return new Rational(this._n * other._d + this._d * other._n, 
			    this._d * other._d);
    }
    
    // O(logN)
    public Rational multiply(Rational rhs) {
	return new Rational(this._n * rhs._n,
			    this._d * rhs._d);
    }

    // post: invert(a/b) -> b/a
    // throws exception if a = 0
    // O(logN)
    public Rational invert() {
	if (this._n == 0)
	    throw new ArithmeticException("Not Invertible");
	return new Rational(this._d, this._n);
    }
    
    // O(log(N)
    public Rational subtract(Rational rhs) {
	return this.add(new Rational(-rhs._n, rhs._d));
    }

    // O(logN)
    public Rational divide(Rational rhs) {
	return this.multiply(rhs.invert());
    }

    
    // overwrite
    public String toString() {
	return _n + "/" + _d;
    }
    
    public Rational negate() {
	Rational ret = new Rational(this._n, this._d);
	ret._n = -ret._n;
	return ret;
    }

    public int getNumerator() {
	return _n;
    }

    public int getDenominator() {
	return _d;
    }

    public boolean equals(Object x) {
	return (x instanceof Rational)
	    && (this._n == ((Rational)x)._n)
	    && (this._d == ((Rational)x)._d);
    }
    
    
    public int compareTo(Comparable rhs) {
	int ad = getNumerator() * ((Rational) rhs).getDenominator();
	int bc = getDenominator() * ((Rational) rhs).getNumerator();
	return ad - bc;
    }

    public static void main(String[] args) {
	//Rational foo = new Rational(4,8);
	//System.out.println(foo);

	/*
	Rational a = new Rational(3,9);
        Rational b = new Rational(a);
	System.out.println(a);  // 3/9
	System.out.println(b); // 3/9
	b = a.negate();
	System.out.println(a);  // 3/9
	System.out.println(b); // -3/9
	*/
	Rational a = new Rational(4,12);
	Rational b = new Rational(8,12);
	/*
	System.out.println(a.add(b));
	System.out.println(a.subtract(b));
	
	System.out.println(a.multiply(b));
	System.out.println(a.divide(b));
	
	System.out.println(a.invert());
	*/
	System.out.println(a.compareTo(b));

	int N = 5;
	Rational[] rats = new Rational[N];
	for (int i = 0; i < N; i++) {
	    int n = (int)(Math.random() * 10) -5;
	    int d = (int)(Math.random() * 9) +1;
	    rats[i] = new Rational(n,d);
	}
	ArrayIO.printArray(rats);
	Sorts.selectionSort(rats);
	ArrayIO.printArray(rats);
    }
}
