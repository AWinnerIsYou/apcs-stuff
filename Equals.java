public class Equals {
    public static void main (String[] args) {
	Integer x = 3;
	Integer y = null;
	String z = "ab";
	System.out.println(x.equals(x)); //true;   x==x (alias checking)
	System.out.println(x.equals(3)); //true;   x.equals(new Integer(3))
	System.out.println(x.equals(y)); //false;  
	System.out.println(y.equals(x)); //R.E;    Null Pointer Exception
	System.out.println(x.equals(z)); //false   Different types
    }
}
