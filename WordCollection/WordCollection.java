import java.util.ArrayList;

public class WordCollection{

    private ArrayList<String> _collection;
       
    // constructor
    // postcondition: creates an empty WordCollection
    public WordCollection(){
	_collection = new ArrayList<String>();
    }

    //postcondition: Copys from the String[] to the WordCollection
    public WordCollection(String [] words){  
	this();
	for (int i = 0 ; i < words.length; i ++) 
	    insert(words[i]);
	
    }

    // returns the total number of items stored in the collection
    public int size() {
	return _collection.size();
    }

    // returns kth word in alphabetical order, where
    // 0 ≤ k < size()
    public String findKth(int k) {
	return _collection.get(k);
    }

    // adds word to the collection (duplicates allowed) by maintaining
    // a sorted(ascending) list of words.
    // O(n)
    public void insert(String word){ 
	int pos = size();
	for (int i = 0 ; i < size(); i++) {
	    if (word.compareTo(findKth(i)) <= 0) {
		pos = i;
		break;
		
	    }
	}
	_collection.add(pos,word);
	
    }
    // returns the index of the first occurrence of word in the collection
    // returns -1 if not found.
    // O(n)
    public int indexOf(String word){
	for (int i = 0; i < size(); i++)
	    if (findKth(i).equals(word))
		return i;
	return -1;
    }
    
    // removes one instance of word from the collection if word is
    // present; otherwise, does nothing
    // O(n)
    public void remove(String word){
	for (int i = 0; i < size(); i++)
	    if (findKth(i).equals(word)) {
		_collection.remove(i);
		break;
	    }
    }
  
    public String toString(){
	return _collection.toString();
    }

    // postcondition: returns the number of occurrences of word in C
    public static int occurrences(WordCollection C, String  word) {
	int ret = 0;
	for (int i = 0; i < C.size(); i++)
	    if (C.findKth(i).equals(word))
		ret++;
	return ret;
    }

    // postcondition: if word is present in C, all but one occurrence
    //is removed; otherwise, C is unchanged
    public static void removeDuplicates(WordCollection C, String word) {
	while (occurrences(C.word) > 1)
	    C.remove(word);
    }
	
    // precondition: C is not empty
    // postcondition: returns the word that appears most often in C;
    //if there is more than one such word, returns any one of those words
    public static String mostCommon(WordCollection  C) {
	String ret = C.findKth(0);
	for (int i = 1; i < C.size(); i++)
	    if (occurrences(C, C.findKth(i)) > occurrences(C, ret))
		ret = C.findKth(i);
	return ret;
    }

    public static void main(String[] args) {
	String[] arr = {"cat","dog","tree","qwerty","aaa","qwerty","qwerty","isis","bb"};
	WordCollection wc = new WordCollection(arr);	

	System.out.println("Size: " + wc.size());	
	System.out.println("Collection: " + wc);
	
	//	wc.remove("qwerty");
	//	System.out.println("Size: " + wc.size());	
	//	System.out.println("Collection: " + wc);
	System.out.println(mostCommon(wc));
    }
    
}
