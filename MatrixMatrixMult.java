public class MatrixMatrixMult{

    public static void print(int[][] a){
	for (int i = 0; i < a.length; i++){
	    for (int j = 0; j < a.length; j++)
		System.out.print(a[i][j] + " ");
	    System.out.println();
	}

    }

    
    public static void main(String [] args){
	int [][] a = {{1,2,3},{4,5,6},{7,8,9}};
	int [][] b = {{0,0,1},{0,1,0},{1,0,0}};
	int N = a.length;
	int [][] c = new int[N][N];


	System.out.println("A");
	print(a);
	System.out.println("B");
	print(b);
	// matrix multiplication: AB = C
        // **********************************
	for (int i = 0; i < N; i++)
	    for (int j = 0; j < N; j++)
	        for (int k = 0; k < N; k++)
                    c[i][j] += (a[i][k])*(b[k][j]);  //***** Write your code here. *****
          
	//***********************************
	System.out.println("AB");
	print(c);
	
    }



}