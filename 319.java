public class DLinkedList{
    
    private DNode _header, _trailer;  // dummy (sentinel) nodes
    private int _size;



    // constructor
    public DLinkedList(){
	_header = new DNode(null, null, null);
	_trailer = new DNode(null,_header, null);
	_header.setNext(_trailer);
	_size  = 0;
    }

    // accessor methods
    public int size(){
	return _size;
    }

    public boolean isEmpty(){
	return _size == 0;
    }

    public DNode getFirst(){
	if (isEmpty()) throw new IllegalStateException("empty list");
	return _header.getNext();
    }

    public DNode getLast(){
	if (isEmpty()) throw new IllegalStateException("empty list");
	return _trailer.getPrevious();
    }


    public DNode getPrevious(DNode current){
	if (current == _header)
	    throw new IllegalArgumentException("cannot move back past header");
	return current.getPrevious();
    }

    public DNode getNext(DNode current){
	if (current == _trailer)
	    throw new IllegalArgumentException("cannot move forward past trailer");
	return current.getNext();

    }

    //*******************************************************************
    // inserts node a before node b. 
    // An exception is thrown if b is the header
    public void addBefore(DNode b, DNode a){
	DNode prevB = getPrevious(b); // throws exception
	prevB.setNext(a);
	a.setNext(b);
	a.setPrevious(prevB);
	b.setPrevious(a);
	_size++;
    }

    public void addLast(DNode node){
	addBefore(_trailer, node);
    }

    public void addLast(String value){
	addLast(new DNode(value,null,null));
    }


    //*******************************************************************
    // inserts node b after node a
    // throw exception if b is the trailer node
    public void addAfter(DNode a, DNode b){
	DNode afterA = getNext(a);
	a.setNext(b);
	b.setPrevious(a);
	afterA.setPrevious(b);
	b.setNext(afterA);
	_size++;
    }


    public void addFirst(DNode current){
	addAfter(_header,current);
    }

    public void addFirst(String value){
	addFirst(new DNode(value,null,null));
    }


    //*******************************************************************
    public String get(int i){
	if (i < 0 || i >= size())
	    throw new IndexOutOfBoundsException("index < 0 || index >= size()");
	DNode current = null;
	if ( i < size() / 2){
	    current = _header;	
	    for (int j = 0; j <= i ; j++){
		current = current.getNext();
		System.out.print("h");
	    }
	}
	else{
	    current = _trailer;
	    for (int j = 0; j < size() - i; j++){
		current = current.getPrevious();
		System.out.print("t");
	    }
	}
	return current.getValue();
    }

    //*******************************************************************
    //precondition: v != null and is a node in the list
    //postconditon: removes the node v refers to.
    //              Throws exception if v is header or trailer.
    public void remove(DNode v){
	DNode prev = getPrevious(v);
	DNode next = getNext(v);
	prev.setNext(next);
	next.setPrevious(prev);
	_size--;
	// unlink the node
	v.setPrevious(null);
	v.setNext(null);
	
    }

    //*******************************************************************
    public String removeFirst(){
	String ans = getFirst().getValue();
	remove(getFirst());
	return ans;
    }

    //*******************************************************************
    public String removeLast(){
	String ans = getLast().getValue();
	remove(getLast());
	return ans;
    }
       

    public boolean hasPrevious(DNode v){
	return v != _header;
    }

    public boolean hasNext(DNode v){
	return v != _trailer;
    }

    // O(n)
    public DNode middleNode(){
    	if (isEmpty()) throw new IllegalStateException();
	return isMid(_header,_trailer);
    }

    public DNode isMid(DNode x, DNode y){
	if (x == y || getNext(x) == y) return y;
	return isMid(getNext(x), getPrevious(y));
    }
    // O(n)
    public DNode middleNode2(){
	if (isEmpty()) throw new IllegalStateException();
	DNode a = _header;
	DNode b = _trailer;
	while (true){
	    if (a == b) return a;
	    a = getNext(a);
	    if (a == b) return a;
	    b = getPrevious(b);
	}
    }


    public String toString(){
	String ans = "";
	DNode current = _header.getNext();
	while (current != _trailer){
	    ans += current.getValue() + ", ";
	    current = current.getNext();
	}
	int len = ans.length();
	if (len > 0) ans = ans.substring(0,len - 2);
	ans = "[" + ans + " ]";
	return ans;
    }
    
    


    public static void main(String [] args){
	DLinkedList L = new DLinkedList();
	System.out.println(L);
	L.addLast("Jane");
	System.out.println(L); // [Jane ]
	L.addLast("Mary"); 
	System.out.println(L); // [Jane, Mary ]
	L.addLast("Zoe");
	System.out.println(L); // [Jane, Mary, Zoe ]
	L.addFirst("Carol");
	System.out.println(L); // [Carol, Jane, Mary, Zoe ]
	L.addFirst("Abe");
	System.out.println(L); // [Abe, Carol, Jane, Mary, Zoe ]
	/*
	  for (int i = 0; i < L.size(); i++){
	  //System.out.println(L.get(i));
	  }
	*/
	while (! L.isEmpty()){ 
	    //System.out.println(L.removeFirst());
	    System.out.println(L.removeLast());
	    System.out.println(L);
	}
	    
    }

}
