public class Odd {
    public static boolean odd(boolean a, boolean b, boolean c) {
	boolean[] abc = {a,b,c};
	boolean[][] boolArr = {{true,false,false},{false,true,false},{false,false,true},{true,true,true}};
	for (int i = 0; i < boolArr.length; i++)
	    if (abc == boolArr[i]) return true;
	return false;

    }

    public static void main(String[] args) {
	boolean a = Boolean.parseBoolean(args[0]);
	boolean b = Boolean.parseBoolean(args[1]);
	boolean c = Boolean.parseBoolean(args[2]);
	System.out.println(odd(a,b,c));
    }


}
