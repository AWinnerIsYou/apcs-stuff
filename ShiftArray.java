import java.util.Arrays;
public class ShiftArray {
    public static void main(String[] args) {
	int[] foo = {1, 3, 7, 11, 15};
	int temp1 = 0;
	int temp2 = foo[0];
	for (int i = 0; i < foo.length; i++) {
	    temp1 = temp2;
	    if (i != foo.length-1) {
		temp2 = foo[i+1];
		foo[i+1] = temp1;
	    }
	    else {
		temp2 = foo[0];
		foo[0] = temp1;
	    }
	}
	System.out.println(Arrays.toString(foo));
    }
}
