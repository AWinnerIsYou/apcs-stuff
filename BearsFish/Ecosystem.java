public class Ecosystem{

    private Animal[] _river;


    public Ecosystem(int size){
	_river = new Animal[size];
    }

    public Ecosystem(int size, int numBears, int numFish){
	this(size);
	while (numBears > 0){
	    int r = (int) (Math.random() * size);
	    if (_river[r] == null){
		_river[r] = new Bear(this,r);
		numBears--;
	    }
	}
	while (numFish > 0){
	    int r = (int) (Math.random() * size);
	    if (_river[r] == null){
		_river[r] = new Fish(this,r);
		numFish--;
	    }
	}
    }

    public Animal[] getRiver(){
	return _river;
    }


    public void step(){
	for (Animal a: _river)
	    if (a != null) a.move();

    }



    public String toString(){
	String ans = "|";
	for (Animal a : _river){
	    if (a != null)
		ans += a;
	    else 
		ans += "_";
	}
	ans += "|";
	return ans;
    }

    public static void main(String [] args){
	Ecosystem eco = new Ecosystem(50,5,10);
	int N = Integer.parseInt(args[0]);
	System.out.println(eco);
	for (int i = 0; i < N; i++){
	    eco.step();
	    System.out.println(eco);
	}
    }



}
