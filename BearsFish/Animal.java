public class Animal implements Edible{
    
    private Ecosystem _eco;
    private Animal[] _river;
    private int _currentPos, _nextPos;


    public Animal(Ecosystem eco, int pos){
	_eco = eco;
	_river = eco.getRiver();
	_currentPos = pos;
    }

    public void move(){
	double r = Math.random();
	if (r < 0.33)
	    _nextPos = _currentPos + 1;
	else if (r < 0.66)
	    _nextPos = _currentPos - 1;
	if (_nextPos < 0 || _nextPos >= _river.length ||
	    _river[_nextPos] != null)
	    _nextPos = _currentPos;
	_river[_currentPos] = null;
	_currentPos = _nextPos;
	_river[_currentPos] = this;
	
    }

    public boolean die(){
	//_river[_pos] = null;
	return false;
    }

    public String toString(){
	return "Animal";
    }


}
