public class HotPotatoGame {
    
    private ArrayQueue<Integer> _circle;
    
    public HotPotatoGame(int n){
	_circle = new ArrayQueue<Integer>(n);
	for (int i = 1; i <= n; i++)
	    _circle.enqueue(i);
    }
    
    public HotPotatoGame(){
	this(10);
    }
    
    public void play(){
	while (_circle.size() != 1) {
	    int current = _circle.dequeue();
	    System.out.println("Kid #" + current + " has the potato");
	    //if ((int)(Math.random() * _circle.size()) != 0) //prob1
	    if (Math.random() > 0.1) //prob2
		_circle.enqueue(current);
	    else {
		System.out.println("Hot Potato!");
		System.out.println("Kid#" + current + " was removed");
		System.out.println(_circle.size() + " kids remaining");
	    }
	    System.out.println();
	}
	System.out.println("Kid #" + _circle.front() + " won");
	
    }

    public static void main(String[] args) {
	HotPotatoGame game = new HotPotatoGame(2);
	game.play();
    }

}
