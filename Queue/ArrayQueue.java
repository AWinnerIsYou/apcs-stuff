public class ArrayQueue<E> implements Queue<E>{

    private int _front, _rear, _size;
    private E[] _queue;
    private final static int CAPACITY = 10000;

    public ArrayQueue(int capacity){
	_front = -1;
	_rear = _size = 0;
	_queue = (E[]) new Object[capacity];
    }

    public ArrayQueue(){
	this(CAPACITY);
    }

    public int size(){
	return _size;
    }
 
    public boolean isEmpty(){
	return size() == 0;
    }
    // O(1)
    public E front() throws EmptyQueueException{
	if (isEmpty())
	    throw new EmptyQueueException("Empty queue");
	return _queue[_front];
    }
    // O(1)
    public E dequeue() throws EmptyQueueException{
	E ans = front();
	_queue[_front] = null;
	_size--;
	if (!isEmpty())
	    _front = (1 + _front) % _queue.length;
	else{
	    _front = -1;
	    _rear = 0;
	}
	return ans;
    }

    // O(1)
    public void enqueue(E value) throws FullQueueException{
	if (size() == _queue.length)
	    throw new FullQueueException("Full Queue");
	_queue[_rear] = value;
	if (isEmpty()) _front = _rear;
	_rear = (1 + _rear) % _queue.length;
	_size++;
    }

    public String toString() {
	if (isEmpty()) return "[]";
	String ans = "[" + _queue[_front];
	for (int i = 1; i < size(); i++) {
	    ans += ", " + _queue[(i + _front) % _queue.length];
	}
	return ans += "]";
    }

    public static void main(String[] args) {
	Queue<Integer> q = new ArrayQueue<Integer>(4);
	q.enqueue(3);
	q.enqueue(5);
	q.enqueue(7);
	q.enqueue(9);
	q.dequeue();
	q.enqueue(11);
	System.out.println(q);
    }


}
