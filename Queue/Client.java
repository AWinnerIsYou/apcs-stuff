public class Client{

    public static<E> void replace(Queue<E> q, E oldValue, E newValue) {
	for (int i = 0; i < q.size(); i++) {
	    E temp = q.dequeue();
	    if (temp.equals(oldValue)) temp = newValue;
	    q.enqueue(temp);
	}
    }
    
    public static void main(String [] args) {
	Queue<Integer> Q = new NodeQueue<Integer>();
	Q.enqueue(1);
	Q.enqueue(2);
	Q.enqueue(3);
	Q.enqueue(2);
	Q.enqueue(4);
	System.out.println(Q);
	replace(Q, 2, 5);
	System.out.println(Q);
	
    }
}
