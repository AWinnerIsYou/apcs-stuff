public class IndexThousand {
    public static void main(String[] args) {
	int[] a = new int[1000];
	System.out.println(a[1000]);
    }
}

/* the program compiles, but at runtime this error appears:
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 1000
	at IndexThousand.main(IndexThousand.java:4)
*/
