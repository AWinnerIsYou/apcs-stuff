import java.util.ArrayList;

public class InsertionSort {
    
    // returns List with integers from 0 to n in random order
    public static ArrayList<Integer> randomList(int n) {
	ArrayList<Integer> ans = new ArrayList<Integer>();
	for (int i = 0; i < n; i++) {
	    ans.add(i);
	}
	for (int j = 0; j < n; j++) {
	    int newInd = (int) (Math.random()*(n));
	    ans.set(j,ans.set(newInd,ans.get(j)));
	}
	return ans;
    }
    
    public static void sort(ArrayList<Integer> L) {
	int N = L.size();
	for (int i = 1; i < N; i++) {
	    System.out.println("start pass: " + i + " " + L);
	    System.out.println(" walk down " + L.get(i));
	    for (int j = i; j > 0; j--) {
		if (L.get(j) < L.get(j-1)) {
		    L.set(j, L.set(j-1, L.get(j)));
		    System.out.println(" " + L);
		}
		else break;
	    }
	}
    }


    public static void main(String[] args) {
	int size = Integer.parseInt(args[0]);
	ArrayList<Integer> list = randomList(size);
	System.out.println("Random: " + list);
	sort(list);
    }

}
