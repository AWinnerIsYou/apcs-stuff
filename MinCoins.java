public class MinCoins {
	public static void main(String [] args) {
		// declare variables
		int cents, quarters, dimes, nickels, pennies;

		// initialize and update the variables
		cents = Integer.parseInt(args[0]);
		quarters = cents / 25;
		cents = cents % 25;
		dimes = cents / 10;
		cents = cents % 10;
		nickels = cents / 5;
		pennies = cents % 5;

		// output
		System.out.println("#quarters: " + quarters);
		System.out.println("#dimes: " + dimes);
		System.out.println("#nickels: " + nickels);
		System.out.println("#pennies: " + pennies);
		System.out.println("#coins: " + (quarters + dimes + nickels + pennies));
	}
}
