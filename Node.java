class Node {
    private String _value;
    private Node _next;

    public Node(String value, Node next) {
	_value = value;
	_next = next;
    }
    
    // accessors
    public String getValue() {
	return _value;
    }

    public Node getNext() {
	return _next;
    }

    // modifiers
    public String setValue(String str) {
	String ans = getValue();
	_value = str;
	return ans;
    }
    
    public Node setNext(Node t) {
	Node ans = getNext();
	_next = t;
	return ans;
    }

    public String toString() {
	return getValue();
    }

    public static void main(String[] args) {
	Node a = new Node("Bill", new Node("Mary", new Node("Sue", null)));
	System.out.println(a);
	System.out.println(a.getNext());
	System.out.println(a.getNext().getNext());
	System.out.println(a.getNext().getNext().getNext());
    }
}


