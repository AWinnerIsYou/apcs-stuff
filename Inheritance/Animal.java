public class Animal {
    protected Ecosystem _river;
    protected int _pos;
    
    public Animal(Ecosystem river, int pos) {
	if (pos < 0 || pos >= river.length()) throw new IllegalArgumentException("Out of bounds");
	_river = river;
	_pos = pos;
	_river[pos] = this;
    }
    
    
    
}

class Bear extends Animal {
    public Bear(Ecosystem river, int pos) {
	super(river, pos);
    }
    
    

}

class Fish extends Animal {
    public Fish(Ecosystem river, int pos) {
	super(river, pos);
    }
    
}

class Ecosystem {
    protected Animal[] _river;
    
    public Ecosystem(int size) {
	_river = new Animal[size];
    }
    
    public int length() {
	return _river.length;
    }

    public void nextStep() {
	
    }
    
}
