public class DNode {
    
    private  _value;
    private DNode _previous, _next;
    
    // constructors
    public DNode(String value, DNode prev, DNode next) {
	_value = value;
	_previous = prev;
	_next = next;
    }
    
    public String toString() {
	return _value;
    }
    
    // accessors
    public String getValue() {
	return _value;
    }

    public DNode getPrevious() {
	return _previous;
    }
    
    public DNode getNext() {
	return _next;
    }

    // modifiers
    public String setValue(String s) {
	String ans = getValue();
	_value = s;
	return s;
    }

    public DNode setPrevious(DNode n) {
	DNode ans = getPrevious();
	_previous = n;
	return n;
    }

    public DNode setNext(DNode n) {
	DNode ans = getNext();
	_next = n;
	return n;
    }
    

    public static void main(String[] args) {
	DNode a = new DNode("Amy", null, null);
	DNode b = new DNode("Bill", a, null);
	DNode c = new DNode("Carol", b, null);

	a.setNext(b);
	b.setNext(c);

	DNode current = a;
	while (current != null) {
	    System.out.println(current);
	    current = current.getNext();
	}

	current = c;
	while (current != null) {
	    System.out.println(current);
	    current = current.getPrevious();
	}
	    
	// swap values (not Nodes) of Amy and Carol
    }

}
