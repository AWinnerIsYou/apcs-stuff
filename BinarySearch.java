public class BinarySearch {
    // precondition: x != null , x is sorted in ascending order.
    // postcondition:return the index position of the key.
    //               return -1 when the key is not found in x
    //                

    /*
    public static int binarySearch(Comparable [] x, Comparable key){
	int N = x.length;
	int low = 0;
	int upper = N;
	
	while (low < upper){
	    int mid = low + (upper - low) / 2;
	    if(x[mid].equals(key)) return mid;
	    if(key.compareTo(x[mid]) < 0) upper = mid;
	    else low = mid + 1;
	}
	return -1;
    }
    */

    public static int binarySearch(Comparable[] x, Comparable key) {
	return binarySearchR(0, x.length, x, key);
    }

    private static int binarySearchR(int low, int upper, Comparable[] x, Comparable key) {
	if (low >= upper) 
	    return -1;
	int mid =  low + (upper - low) / 2;
	if (x[mid].equals(key))
	    return mid;
	if (key.compareTo(x[mid]) < 0)
	    return binarySearchR(low, mid, x, key);
	return binarySearchR(mid + 1, upper, x, key);
    }
	
    public static void main (String[] args) {
	Integer[] x = {3,6,10,15,16};
	System.out.println(binarySearch(x,3));
	System.out.println(binarySearch(x,6));
	System.out.println(binarySearch(x,10));
	System.out.println(binarySearch(x,15));
	System.out.println(binarySearch(x,16));
    }
}
