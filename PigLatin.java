public class PigLatin{

    //precondition: w != null and w is not empty
    //
    public static boolean beginsWithVowel(String w, String vowels){
	if (vowels == null) return false;
	return vowels.indexOf(w.substring(0,1)) != -1;
    }
    
    public static int firstVowelPos(String w) {
	for (int i = 1; i < w.length(); i++){
	    String chr = w.substring(i,i+1);
	    if ("aeiouy".indexOf(chr) != -1) return i;
	}
	return -1;
    }
    
		    
    // precondition: w may be null or may be an empty string
    //               w contains only lower case letters
    public static String pigLatin(String w){
	if (w == null || w.length() == 0)
	    return "";
	if (beginsWithVowel(w,"aeiou"))
	    return w + "way";
	if (firstVowelPos(w) != -1)
	    return w.substring(firstVowelPos(w)) + w.substring(0,firstVowelPos(w)) + "ay";
	else return w + "ay";
    }

    public static void main(String [] args){

	while(!StdIn.isEmpty()){
	    String word = StdIn.readString();
	    System.out.println(word + ":\t" + pigLatin(word));
	}
	System.out.println();
    }
    
}
