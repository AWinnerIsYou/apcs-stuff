public class CheckSum{
    /*
      From Sedgewick and Wayne section 2.1 exercise 15
    */


    // *********** Question 1 ************************/
    //precondition: 0 <= d < 10
    //postcondition: returns the sum of the digits of 2d
    //         f(7) = 5, f(1) = 2, ...
    public static int f(long d){
	return (int) ((2*d) % 10 + (2*d) / 10);
    }
    
    // ************ Question 2 ************************/
    
    // precondition: n is a 10 digit number. d0d1d2...d9
    // postcondition: compute the check digit such that the sum:
    // d0 + f(d1) + d2 + f(d3) + d4 +
    // f(d5) + d6 + f(d7) + d8 + f(d9) + checkDigit
    // is a multiple of 10
    // Note: 0 <= checkDigit < 10
    public static int checkDigit(long n){
	long temp= n;
	int weirdSum = 0;
	for (int i = 9; i >= 0; i--) {
	    if (i % 2 == 1)
		weirdSum += f(temp % 10);
	    else
		weirdSum += temp % 10;
	    temp = temp / 10;
	}
	return (10 - (weirdSum % 10))%10;
    }


    // **************** Question 3 ********************/
    // post condition: returns a uniform random 10 digit integer
    
    public static long randomAccount(){
	return (long) (Math.random()*(1e10 - 1e9) + 1e9);
    }

    // ***************** Question 4 *******************/
    // postcondition: returns an 11 digit number where the 
    // least significant digit is the checkdigit
    public static String createAccount(){
	long hai = randomAccount();
	return hai + "" + checkDigit(hai);
    }

    // ***************** Question 5 ******************/
    // requires using String methods such as length(),
    // substring()

    // precondition: acct represents a potential account number 
    // postcondition: returns true if the acct represents an actual account
    public static boolean checkSum(String acct){
	if (length(acct) != 11) return false;
	return (checkDigit(Long.parseLong(acct.substring(0,10))) == Integer.parseInt(acct.substring(10)) );
    }

    public static void main(String [] args){
	// test your functions here 
	System.out.println(checkSum(145837));
    }
}

