public class ScoresArray{

    private GameEntry[] _scores;

    public ScoresArray(int n){
	_scores = new GameEntry[n];
	for(int i = 0; i < n; i++){
	    _scores[i]  = new GameEntry();
	}
    }

    public ScoresArray(){
	this(10);
    }


    // O(n)
    // returns true if entry is added
    // returns false otherwise
    // if an entry is added, the entries with a lower rank are shifted
    // down.
    public boolean add(GameEntry entry){
	GameEntry current = entry;
	for (int i = 0; i < _scores.length; i++) {
	    if (current.compareTo(_scores[i]) > 0) {
		GameEntry temp = current;
		current = _scores[i];
		_scores[i] = temp;
	    }
	}
	return current != entry;
    }

    public boolean addBS(GameEntry entry) {
	
	return false;
    }

    public int numScores(){
	return _scores.length;
    }
    
    public GameEntry topEntry(){
	return _scores[0];
    }

    public String toString(){
	String ans = "Rank\tInitials.....Score\n";
	for (int i = 1; i < _scores.length + 1; i++){
	    ans += i + ".\t" + _scores[i - 1] + "\n";
	}
	return ans;
    }

    // post: returns a rand integer from [0,max)
    public static int randomScore(int max) {
	return (int)(max*Math.random());
    }

    // post: use only capital letters, returns string of length len
    public static String randomInitials(int len) {
	String ans = "";
	String alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (int i = 0; i < len; i++) {
	    int ind = (int) (26*Math.random());
	    ans += alpha.substring(ind, ind+1);
	}
	return ans;
    }

    // post : returns a random GameEntry where 0 <= score < maxScore,
    // _initials.length() = len
    public static GameEntry randomEntry(int maxScore, int len) {
	return new GameEntry(randomInitials(len), randomScore(maxScore));
    }
    
    public void initialize(int maxScore, int len) {
	for (int i = 0; i < _scores.length;i++) {
	    GameEntry entry = randomEntry(maxScore,len);
	    
	    if (add(entry))
		System.out.println("added " + entry);
	    else
		System.out.println("did not add " + entry);
	}
    }

    public static void main(String [] args){
	ScoresArray s = new ScoresArray();
	System.out.println(s);

	for (int i = 0; i < 10000; i++) 	s.initialize(10001,3);
	System.out.println(s);
    }

}
