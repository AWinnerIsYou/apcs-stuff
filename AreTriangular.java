public class AreTriangular {
    public static boolean areTriangular(double a, double b, double c) {
	if (a > (b+c)) return false;
	if (b > (a+c)) return false;
	if (c > (a+b)) return false;
	return true;
    }
    public static void main(String[] args) {
	int a = Integer.parseInt(args[0]);
	int b = Integer.parseInt(args[1]);
	int c = Integer.parseInt(args[2]);
	System.out.println(areTriangular(a,b,c));

    }
}
