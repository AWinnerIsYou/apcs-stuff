public class SkiJumper implements Flier, Athlete, Comparable {
    
    private String _firstName, _lastName;
    private double _hoursTraining;
    private int _numberOfJumps;

    public SkiJumper(String first, String last) {
	_firstName = first;
	_lastName = last;
    }

    public void fly() {
	_numberOfJumps++;
    }

    public void train(double hours) {
	_hoursTraining += hours;
    }

    public double getHoursTrained() {
	return _hoursTraining;
    }

    public int getJumps() {
	return _numberOfJumps;
    }
    
    public int compareTo(Object other) {
	return this.getJumps() - ((SkiJumper)other).getJumps();
    }
}

