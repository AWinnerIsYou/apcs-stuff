public class SkiJumperClient {
    public static void main (String[] args) {
	SkiJumper foo = new SkiJumper("Foo","Johnson");
	SkiJumper bar = new SkiJumper("Bar","Jackson");
	
	//17 jumps
	for (int i = 0; i < 17; i++)
	    foo.fly();
	System.out.println(foo.getJumps());

	/*
	foo.train(35);
	foo.train(20);
	System.out.println(foo.getHoursTrained()); //ret 55.0
	*/

	//9 jumps
	for (int i = 0; i < 9; i++)
	    bar.fly();
	System.out.println(bar.getJumps());
	
	System.out.println(foo.compareTo(bar));


    }
}
    
