public class MaxMin {
    public static void main(String[] args) {
    


        // first value read initialized min and max
        int max = 0;
        int min = Integer.MAX_VALUE;
    
	System.out.print("enter positive numbers or <ctrl-d> to quit: ");
        // read in the data, keep track of min and max
        while (!StdIn.isEmpty()) {
	   
            int value = StdIn.readInt();
	    if (value <= 0) System.out.println("Only positive numbers. ");
	    else{
		if (value > max) max = value;
		if (value < min) min = value;
	    }
	    System.out.print("enter positive numbers: ");
        }
	
        // output
	
	System.out.println("\nmaximum  = " + max + ", minimum = " + min);
    }
}
