/**Use a CircleList to build a game that simulates the classic Children's game
Duck, Duck, Goose.

In the game, the children sit in a circle. A child is chosen at random to 
be designated as "IT". "It" is removed from the circle then walks around
the circle declaring each child A "Duck" or a "Goose". If a child is declared
a "Goose", the "Goose" is removed from the circle. The "Goose's" position is
noted, then the "IT" and "Goose" race around the circle, whoever reaches 
the noted position first, occupies the noted position. The other child
remains as "IT" or becomes "IT" for the next round.

Ex. 

kids: Abe, Cal, Jen, Eve, Pam, Vern, Joe, Quin, Yuki
it: Jen
kids: Cal, Eve, Pam, Vern, Joe, Quin, Yuki, Abe 
Duck : Cal      
Duck : Eve
Duck: Pam
Goose: Vern
Race between Jen and Vern
kids:  Pam, Jen, Joe, Quin, Yuki, Abe, Cal, Eve
it : Vern

and the game continues.
*/

import java.util.ArrayList;

public class DuckDuckGoose{

    private CircleList _circle;

    // *** Question 4 ****
    // Transfer the names from the ArrayList into a CircleList.
    public DuckDuckGoose(ArrayList<String> names){
	_circle = new CircleList();
	for (String name : names)
	    _circle.add(new Node(name,null));
    }



    // *** Question 5 ****
    // Returns a randomly selected child to be It.
    // The child is removed from the circle and its node is returned.
    public Node chooseIt(){
	int rand = (int) (Math.random()*_circle.size());
	for (int i = 1 ; i < rand; i++)
	    _circle.advance();
	return _circle.remove();
    }

    public String toString() {
	return _circle.toString();
    }

    public void play(int n) {
	System.out.println("kids:\t" + this + "\n");
	Node it = chooseIt();

	for (int i = 0; i < n; i++) {
	    Node goose = _circle.getCursor();
	    
	    System.out.println("kids:\t" + this);
	    System.out.println("it:\t" + it);
	    while (true) {
		if (Math.random() < 0.25) {
		    System.out.println("goose:\t" + goose);
		    break; 
		}
		else {
		    System.out.println("duck:\t" + goose);
		    goose = goose.getNext();
		}
	    }
	    System.out.println("Race between:\t" + it + " and " + goose);
	    if (Math.random() < .5) {
		System.out.println(it  + " wins the race");
		_circle.add(it);
		
		while (_circle.getCursor() != goose) 
		    _circle.advance();
		
		it = _circle.remove();
	    }
	    else {
		System.out.println(goose + " wins the race");
	    }
	    System.out.println("kids:\t" + this);
	    System.out.println();
	}
	
    }
    
    
    public static void main(String [] args){
	String [] n = {"Abe","Cal","Jen","Eve","Pam","Vern", "Joe", "Quin", "Yuki"};
	// *** Question 1 *** 
	// Declare and instanciate an ArrayList of strings.
	ArrayList<String> L = new ArrayList<String>();

	// *** Question 2 ***
	// Copy the items of the String [] into an ArrayList of Strings.
	for (String s : n)
	    L.add(s);

	// *** Question 3 ****
	// Declare and instantiate a DuckDuckGoose object.
	// The constructor's argument will be an ArrayList of Strings.    
	DuckDuckGoose game = new DuckDuckGoose(L);
	game.play(2);
    }



}

