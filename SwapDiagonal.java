public class SwapDiagonal {

    public static void printArray(int[][] a) {
	for (int i = 0; i < a.length; i++) {
	    for (int j = 0; j < a.length; j++) System.out.print(a[i][j] + " ");
	    System.out.println();
	}
    }

    public static void main(String[] args) {
	int[][] a = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
	int N = a.length;
	if (N > 1) {
	    for (int i = 1; i < N; i++) {
		for (int j = 0; j < i; j++) {
		    int temp = a[i][j];
		    a[i][j] = a[j][i];
		    a[j][i] = temp;
		}
	    }
	}
	printArray(a);
    }
}