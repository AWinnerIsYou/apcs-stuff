public class DivisorPatterns {
    public static void main(String[] args) {
	int num = Integer.parseInt(args[0]);
	int i = 1;
	int j = 1;
	String ans = "";
	while (i <= num) {
	    while (j <= num) {
		if ((i % j == 0) || (j % i) == 0) ans += "*";
		else ans += " ";
		j++;
	    }
	    ans += i + " \n";
	    j = 1;
	    i++;
	}
	System.out.print(ans);
    }
}
