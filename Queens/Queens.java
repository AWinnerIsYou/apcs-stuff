/*
  Consider the Eight (N) Queens problem. It requires determining the
  various ways in which eight queens could be configured on a chessboard
  so that none of them could capture any other queen. (The rules of 
  chess allow a queen to move in an arbitrary number of squares in 
  horizontal, vertical, or diagonal fashion.) The figure below illustrates
  one such configuration.
  
  - - - - - - - Q
  - - - Q - - - -
  Q - - - - - - -
  - - Q - - - - -
  - - - - - Q - -
  - Q - - - - - -
  - - - - - - Q -
  - - - - Q - - -
  
*/

public class Queens {
    private boolean[][] _board;
    private int _countSol;

    public Queens() {
	_board = new boolean[8][8];
	_countSol = 0;
    }

    // start at row 0 and go down rows
    public void start() {
	checkRow(0);
    }
	
    // i: row
    private void checkRow(int i) {
	for (int j = 0; j < 8; j++) { //j: col
	    // place queen
	    _board[i][j] = true;
	    
	    
	    if (i < 7) checkRow(i+1);
	    else if (goodBoard()) { //last row: check if board passes test
		printBoard();
		
		try {
		    Thread.sleep(100);                 //1000 milliseconds is one second.
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		
		_countSol++;
	    }
	    
	    // reset row
	    _board[i][j] = false;
	}	
    }

    private boolean goodBoard() {
	for (int i = 0; i < 8; i++) {
	    for (int j = 0; j < 8; j++) {
		if (_board[i][j]) { //if a queen in place
		    //column check
		    for (int k = i+1; k < 8; k++) //go down the column
			if (_board[k][j]) return false; //queen in the way -> board doesnt pass test

		    //diagonal check
		    int shift = 1; //row and col shift by same num
		    //up and left
		    while (isInBounds(i - shift, j - shift)) {
			if (_board[i-shift][j-shift]) return false; //queen in the way
			shift++; //go across diagonal
		    }
		    shift = 1; //reset shift

		    // up and right
		    while (isInBounds(i - shift, j + shift)) {
			if (_board[i-shift][j+shift]) return false;
			shift++;
		    }
		    shift = 1;

		    //down and left
		    while (isInBounds(i + shift, j - shift)) {
			if (_board[i+shift][j-shift]) return false;
			shift++;
		    }
		    shift = 1;

		    //down and right
		    while (isInBounds(i + shift, j + shift)) {
			if (_board[i+shift][j+shift]) return false;
			shift++;
		    }
 		    shift = 1;

		    //if no queens in the way, go to the next row
		    break;
		}
	    }
	}
	return true; //board has passed all tests
    }

    public static boolean isInBounds(int i, int j) { //check if _board[i][j] is not out of bounds
	return (i >= 0 && j >= 0 && i < 8 && j < 8);
    }

    public void printBoard() {
	for (int i = 0; i < 8; i++) {
	    for (int j = 0; j < 8; j++) {
		if (_board[i][j]) System.out.print("Q");
		else System.out.print("-");
		System.out.print(" ");
	    }
	    System.out.println();
	}	
	System.out.println();
    }

    public int getSolutionsNum() { //number of solutions counted
	return _countSol;
    }

    public static void main(String[] args) {
	Queens hi = new Queens();
	hi.start();
	System.out.println(hi.getSolutionsNum() + " Solutions");
    }
}
