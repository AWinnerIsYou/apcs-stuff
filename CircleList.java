public class CircleList {
    
    private Node _cursor;
    private int _size;
    
    public CircleList() {
	_cursor = null;
	_size = 0;
    }

    public int size() {
	return _size;
    }

    public boolean isEmpty() {
	return size() == 0;
    }

    public Node getCursor() {
	return _cursor;
    }

    /** adds newNode after _cursor
     */
    public void add(Node newNode) {
	if (isEmpty()) {
	    _cursor = newNode;
	    _cursor.setNext(_cursor);
	}

	else {
	    newNode.setNext(_cursor.getNext());
	    _cursor.setNext(newNode);
	}
	
	_size++;
    }

    public String toString() {
	if (isEmpty())
	    return "[]";
	String ans = "[" + _cursor;
	Node curr = _cursor.getNext();
	while (curr != _cursor) {
	    ans += ", " + curr;
	    curr = curr.getNext();
	}
	return ans + "]";
    }

    public void advance() {
	_cursor = _cursor.getNext();
    }

    public Node remove() {
	if (size() == 1) {
	    Node ans = _cursor;
	    ans.setNext(null);
	    
	    _cursor = null;
	    _size = 0;
	    return ans;
	}
	
	Node ans = _cursor; //ans
	Node prev = _cursor; //pointing to cursor
	while (prev.getNext() != _cursor)
	    prev = prev.getNext();
	prev.setNext(_cursor.getNext()); //chain
	_cursor.setNext(null); //unlink
	_cursor = prev.getNext(); //set new cursor
	_size--; //dec
	return ans;
    }
    
    public static void main(String[] args) {
	CircleList L = new CircleList();
	
	L.add(new Node("Abe", null));
    	L.add(new Node("Betty", null));
	L.advance(); //_cursor = Betty
	L.add(new Node("Ada", null));
	L.advance(); //_cursor = Ada
	L.advance(); //_cursor = Abe

	System.out.println("size:\t" + L.size());
	System.out.println("cursor:\t" + L.getCursor());
	System.out.println("list:\t" +  L.toString());
	System.out.println();
	System.out.println("node removed:\t" + L.remove());
	System.out.println("list:\t" +  L.toString());
	System.out.println("new cursor:\t" + L.getCursor());
    }

}
