// Arithmetic Progression : a, a + r, a + 2r, ...

public class ArithmeticProgression extends Progression{

    private int _inc;

    public ArithmeticProgression(int increment){
	super();
	_inc = increment;
    }

    // override the method by redefining it
    public int nextValue(){
	_curr += _inc;
	return _curr;
    }

}
