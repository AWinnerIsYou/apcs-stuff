// Geometric Progression: a, ar, ar^2, ar^3, ...

public class GeometricProgression extends Progression{

    private int _base;

    public GeometricProgression(int base){
	super();
	_base = base;
    }

    public GeometricProgression(){
	super();
	_base = 2;
    }

    public int nextValue(){
	_curr *= _base;
	return _curr;
    }

}
